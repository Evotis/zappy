/*
** see.h for server in /home/abd-el_y/rendu/psu_2013_zappy/server
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Thu Jul 10 11:10:02 2014 Abd-el rahman
** Last update Thu Jul 10 11:11:46 2014 Abd-el rahman
*/

#ifndef SEE_H_
# define SEE_H_

typedef struct	s_word
{
  t_citem	citem;
  char		*str;
}		t_word;

#endif /* !SEE_H_ */
