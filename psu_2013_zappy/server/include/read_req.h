/*
** read_req.h for include in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/include
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sun Jul 13 02:31:14 2014 thomas nieto
** Last update Sun Jul 13 02:31:14 2014 thomas nieto
*/

#ifndef READ_REQ_H_
# define READ_REQ_H_

int		read_req(void);

#endif /* !READ_REQ_H_ */

