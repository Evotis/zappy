/*
** server_time.h for include in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/include
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Jul 07 15:34:41 2014 thomas nieto
** Last update Sat Jul 12 21:42:07 2014 Abd-el rahman
*/

#ifndef SERVER_TIME_H_
# define SERVER_TIME_H

# include "server.h"

# define NOW 0

typedef enum	e_delay
{
  AVANCE = 7,
  DROITE = 7,
  GAUCHE = 7,
  VOIR = 7,
  INVENTAIRE = 1,
  PREND = 7,
  POSE = 7,
  EXPULSE = 7,
  BROADCAST = 7,
  INCANTATION = 300,
  FORK = 42,
  CONNECT = 0,
  EAT = 126,
  DFOOD = 100,
  DLINEMATE = 150,
  DDERAUMERE = 200,
  DSIBUR = 250,
  DMENDIANE = 300,
  DPHIRAS = 400,
  DTHSTAME = 500,
  DTHYSTAME = 700
}		t_delay;

long long int		get_resp_time(t_delay);
long long int		get_current_time(void);

#endif /* SERVER_TIME_H */
