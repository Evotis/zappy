/*
** server.h for Epitech in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by thomas nieto
** Login   <thomas.nieto@epitech.net>
**
** Started on Sat Jun 21 23:31:10 2014 thomas nieto
** Last update Sat Jul 12 19:12:12 2014 Abd-el rahman
*/

#ifndef SERVER_H_
# define SERVER_H_

# include <sys/socket.h>
# include <netdb.h>
# include <arpa/inet.h>
# include <netinet/in.h>
# include <sys/types.h>

# include "data.h"
# include "circular_buffer.h"

/*
** used for map
*/
typedef unsigned short	t_length;

/*
** used for time
*/
typedef unsigned short	t_time;

/*
** team mgmt
*/
typedef struct		s_team
{
  char			*name;
  unsigned short	nb;
}			t_team;

/*
** opt mgmt
*/
typedef struct		s_opt
{
    in_port_t		port;
    t_length		x;
    t_length		y;
    t_team		**team;
    t_time		time_unit;
    unsigned short		nb_client;
    int			debug;
}			t_opt;

/*
** used for socket mgmt
*/
# define DOMAINE AF_INET
# define PROTO "tcp"
# define TYPE SOCK_STREAM
# define BACKLOG 127

int			create_socket(int *);
int			bind_socket(t_info *);
int			listen_connection(int);
int			close_socket(int);

/*
** printers in debug mode
*/
void			print_args(void);
void			print_infos(t_info *);
void			print_map(void);
void			print_players(void);
void			print_player(int);

/*
** globals for server
*/
extern t_map		**g_map;
extern t_player		g_player[BACKLOG];
extern t_opt		g_opt;
extern t_info		g_graphic_client;
extern unsigned short	g_nb_connected;
extern t_server		g_server;
extern t_cbuffer	*g_resp_clients;
extern long long int	g_delay_item[7];

/*
** connection acceptation
** found in connection_procedure.c
*/
# define WELCOME "BIENVENUE\n"
# define WELCOME_DEBUG "BIENVENUE"
# define WELCOME_LEN 10
# define IS_GRAPHIC "GRAPHIC\n"

typedef enum		e_type
{
  G_CLIENT,
  P_CLIENT
}			t_type;

int			accept_connection(void);
int			handle_connection(void);
void			destroy_player(int);
int			life_player_gestion(void);
void			death_player(int);
void			init_delay_item(void);
int			item_appearance_gestion(void);

/*
** routine for game
*/
int			routine(void);
int			monitor_fds(void);

/*
** err code:
**
**   1 - wrong port format
**   2 - wrong width format
**   3 - wrong length format
**   4 - malloc failure
**   5 - wrong time unit format
**   6 - wrong nb client
**   7 - unable to retrieve protocol name
**   8 - creation socket failure
**   9 - bind socket failure
**  10 - listen connection failure
**  11 - Write failure
**  12 - accept failure
**  13 - identification failure
**  14 - no more space on board
**  15 - the requested team name doesn't exist
**  16 - Open failure
**  17 - Read failure
** 255 - close error failure (socket/file/pipe...)
*/

#endif /* !SERVER_H_ */
