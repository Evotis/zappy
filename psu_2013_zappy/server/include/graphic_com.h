 /*
** graphic_com.h for graphic_com in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server/src
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Wed Jun 25 16:49:38 2014 Adrien Chataignoux
** Last update Tue Jul  8 18:22:46 2014 Adrien Chataignoux
*/

#ifndef GRAPHIC_COM_H_
# define GRAPHIC_COM_H_

typedef struct	s_com
{
  char		*key;
  int		(*ptr)();
}		t_com;

/*
** graphic_com.c
*/
int		launch_actions(char *instruction, int playernum);
int		map_content(int fd, int playernum);
int		player_incant(int fd, int playernum);
int		player_egg(int fd, int playernum);
int		player_starvation(int fd, int playernum);

/*
** graphic_actions.c
*/
int		new_player(int fd, int playernum);
int		expulsion(int fd, int playernum);
int		player_inventory(int fd, int playernum);
int		player_pos(int fd, int playernum);
int		player_level(int fd, int playernum, char *instruction);

/*
** graphic_string.c
*/
int		player_broadcast(int fd, int playernum, char *instruction);
int		player_incant_end(int fd, int playernum, char *instruction);
int		player_drop(int fd, int playernum, char *instruction);
int		player_take(int fd, int playernum, char *instruction);
int		egg_laid(int fd, int playernum, char *instruction);

/*
** graphic_string.c
*/
int		egg_birth(int fd, int playernum, char *instruction);
int		egg_connect(int fd, int playernum, char *instruction);
int		egg_die(int fd, int playernum, char *instruction);
int		time_ask(int fd, int playernum, char *instruction);
int		team_win(int fd, char *instruction);

/*
**  graphic_com2.c
*/
int		map_case(int fd, int playernum, char *instruction);
int		map_size(int fd);
int		team_names(int fd);
int		wrong_param(int fd);
int		unknown_cmd(int fd);

/*
** graphic_start.c
*/
int		graphic_start();

char		*build_string(char *str, int *i);
char		*extract_broadcast(char *str);
void		extract_pos(int *x, int *y, char *instruction);

#endif /* !GRAPHIC_COM_H_ */
