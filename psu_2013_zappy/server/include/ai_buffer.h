/*
** ai_buffer.h for ai_buffer in /home/fest/project/sysu/zappy/PSU_2013_zappy/server/src
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Mon Jul  7 12:01:09 2014 Adrien Chataignoux
** Last update Tue Jul  8 17:37:15 2014 Adrien Chataignoux
*/

#ifndef AI_BUFFER_H_
# define AI_BUFFER_H_

# include "circular_buffer.h"

typedef enum    e_request
  {
    MOVE,
    RIGHT,
    LEFT,
    SEE,
    INVENT,
    TAKE,
    DROP,
    EXPEL,
    BROAD,
    INCANT,
    FORKE,
    CONNECTT,
    DEFAULT
  }		t_request;

typedef enum    e_citem
  {
    FOOD,
    LINEMATE,
    DERAUMERE,
    SIBUR,
    MENDIANE,
    PHIRAS,
    THYSTAME,
    DEFAUT
  }		t_citem;

typedef struct		s_reqtype
{
  char			*name;
  t_request		type;
}			t_reqtype;

typedef struct		s_ittype
{
  char			*name;
  t_citem		type;
}			t_ittype;

typedef struct		s_aidata
{
  t_request		request;
  t_citem		citem;
  char			broadcast[LEN];
}			t_aidata;

typedef struct		s_aibuffer
{
  size_t		len;
  t_aidata		**aidata;
  size_t		write;
  size_t		read;
}			t_aibuffer;

t_aibuffer		*ai_create(size_t);
void			ai_free(t_aibuffer *);
int			ai_is_full(t_aibuffer *);
int			ai_push(t_aibuffer *, char *);
int			ai_push_broadcast(t_aibuffer *, char *);
int			ai_push_special(t_aibuffer *, char *);
t_aidata		*ai_pop(t_aibuffer *);
t_aidata		*ai_read(t_aibuffer *);
void			ai_dump(t_aibuffer *);

#endif /* !AI_BUFFER_H_ */
