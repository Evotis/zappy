/*
** broadcast.h for zappy in /home/abd-el_y/work/zappy
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Sun Jun 29 19:11:09 2014 Abd-el rahman
** Last update Wed Jul  9 19:57:52 2014 Abd-el rahman
*/

#ifndef BROADCAST_H_
# define BROADCAST_H_

# include <sys/select.h>
# include <unistd.h>

# include "ai_buffer.h"
# include "server_time.h"
# include "circular_buffer.h"
# include "data.h"
# include "server.h"

# define POS_BROADCAST(angle) (((angle) + (M_PI) / 8) / (2 * (M_PI) / 8) + 1)

typedef struct	s_vec
{
  int		x;
  int		y;
}		t_vec;

typedef struct	s_ptr_vec
{
  t_direct	dir;
  int		x;
  int		y;
}		t_ptr_vec;

#endif /* !BROADCAST_H_ */
