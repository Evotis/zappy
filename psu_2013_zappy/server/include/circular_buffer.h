/*
** circular_buffer.h for include in /home/n0t/dev/circular_buffer/include
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sat Jun 28 11:30:40 2014 thomas nieto
** Last update Wed Jul  9 19:02:24 2014 Abd-el rahman
*/

#ifndef CIRCULAR_BUFFER_H_
# define CIRCULAR_BUFFER_H_

# include <unistd.h>

# define CB_FULL -1

# define LEN 4096

/*
** data structure allows us to handles reponse for clients
** here sockfd refer to the client's fd to whom buf has to be
** written.
**
** the tv structure is the date when the message `buf` has to be
** delivered by the server. And buf is the message
** ALL notify that the message has to written for to every connected
** clients and NOP is to initialize
**
** `len` is the maximum size of the circular buffer
** `head` and `tail`, respectivly indicate where to write and where to read
** in the buffer but in cpp, it would probably be private attributes.
** That means the user doesn't have to care about theses variables
*/
# define ALL -2
# define NOP -3

# define READ 1

# define RESP_PLAYER_POS 0
# define RESP_GRAPHIC_POS 1

typedef struct		s_data
{
  int			sockfd;
  char		read;
  long long int		resp_time;
  char			resp[2][LEN];
}			t_data;

typedef struct		s_cbuffer
{
  size_t		len;
  t_data		**data;
  size_t		w;
  size_t		r;
}			t_cbuffer;

t_cbuffer		*cb_create(size_t);
void			cb_free(t_cbuffer *);
int			cb_push(t_cbuffer *, t_data *);
t_data			*cb_read(t_cbuffer *);
void			cb_dump(t_cbuffer *);
int			cb_is_full(t_cbuffer *);
t_data		*cb_get(t_cbuffer *, size_t);
int		is_empty(const char * const);
void		cb_clear(int);

/*
** not yet implemented
*/
t_data			*cb_pop(t_cbuffer *);

#endif /* !CIRCULAR_BUFFER_H_ */
