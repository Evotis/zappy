/*
** map_gestion.h for server in /home/abd-el_y/rendu/psu_2013_zappy/server
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Sat Jul 12 18:57:47 2014 Abd-el rahman
** Last update Sat Jul 12 19:18:16 2014 Abd-el rahman
*/

#ifndef MG_H_
# define MG_H_

typedef struct	s_delay_tab
{
  t_citem	citem;
  t_delay	delay;
  int		freq;
}		t_delay_tab;

#endif /* !MG_H_ */
