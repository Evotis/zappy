/*
** player_management.h for player_management in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Fri Jun 27 12:27:43 2014 Adrien Chataignoux
** Last update Fri Jun 27 14:43:02 2014 Adrien Chataignoux
*/

#ifndef PLAYER_MANAGEMENT_H_
# define PLAYER_MANAGEMENT_H_

# define FULL -1

int			*search_incant(int x, int y);
int			find_player_free(void);
int			find_player_from_fd(int);

#endif /* !PLAYER_MANAGEMENT_H_ */
