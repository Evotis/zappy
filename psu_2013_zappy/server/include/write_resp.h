/*
** write_resp.h for include in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/include
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sat Jul 12 18:53:41 2014 thomas nieto
** Last update Sat Jul 12 18:53:41 2014 thomas nieto
*/

#ifndef WRITE_RESP_H_
# define WRITE_RESP_H_

void		write_resp(void);

#endif /* !WRITE_RESP_H_ */

