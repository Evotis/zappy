/*
** main.h for Epitech in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by nieto_t
** Login   <thomas.nieto@epitech.net>
**
** Started on Sat Jun 21 15:06:55 2014 thomas nieto
** Last update Mon Jun 23 22:33:10 2014 thomas nieto
*/

#ifndef MAIN_H_
# define MAIN_H_

# include <arpa/inet.h>

# include "server.h"

/*
** option structure
** stored as global from main.c
*/
# define OPTNB		7
# define DEBUG		g_opt.debug

typedef struct		s_opt_tab
{
    char		opt_char;
    int			(*fct)(char *, char *[]);
}			t_opt_tab;

int			get_port(char *, char *[]);
int			get_x(char *, char *[]);
int			get_y(char *, char *[]);
int			get_player(char *, char *[]);
int			get_team_name(char *, char *[]);
int			get_time_unit(char *, char *[]);
int			get_nb_client(char *, char *[]);
int			debug(char *, char *[]);

#endif /* !MAIN_H_ */
