/*
** data.h for zappy_server in /home/abd-el_y/work/zappy
**
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
**
** Started on  Sun Jun 21 15:01:43 2014 Abd-el rahman
** Last update Sat Jul 12 15:02:10 2014 Abd-el rahman
*/

#ifndef DATA_H_
# define DATA_H_

# define MAP 0
# define PLAYER 1

# include <sys/select.h>
# include <sys/socket.h>
# include <sys/types.h>
# include <netdb.h>

# include "ai_buffer.h"

typedef unsigned char	t_quant;
typedef unsigned short	t_coord;

typedef struct		s_info
{
    int			sockfd;
    struct sockaddr_in	addr;
}			t_info;

typedef struct		s_server_info
{
  t_info		info;
  fd_set		readfds;
  fd_set		writefds;
}			t_server;

/*
** -----------------
** | map's section |
** -----------------
**
** define map datas
*/
typedef struct		s_pos
{
  t_coord		x;
  t_coord		y;
}			t_pos;

typedef struct		s_item
{
  t_quant		food;
  t_quant		linemate;
  t_quant		deraumere;
  t_quant		sibur;
  t_quant		mendiane;
  t_quant		phiras;
  t_quant		thystame;
}			t_item;

typedef struct		s_map
{
  t_item		item;
}			t_map;

void			init_item(char, t_item *);
int			init_map(void);

/*
** --------------------
** | player's section |
** --------------------
**
** enum directions for a player
*/
typedef enum		e_direction
{
  NORTH,
  EAST,
  SOUTH,
  WEST
}			t_direct;

/*
** sockets state for fd
**
** close: can't suitable for a client
** open: can be use for a client
** else: busy by a client
*/
# define SOCK_CLOSE -2
# define SOCK_OPEN -1

typedef struct		s_player
{
  int			id_incant;
  t_pos			pos;
  t_info		info;
  t_item		bag;
  unsigned short	num;
  unsigned int		level;
  char			life;
  char			*team_name;
  t_direct		direction;
  t_aibuffer		*ai_bc;
  long long int		need_eat;
}			t_player;

int			init_players(void);
int			init_player(t_info *, int, int);
int			init_one_player(int);
void			player_free(void);

#endif /* !DATA_H_ */
