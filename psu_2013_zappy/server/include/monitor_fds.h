/*
** monitor_fds.h for include in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/include
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Thu Jul 10 17:06:32 2014 thomas nieto
** Last update Thu Jul 10 17:06:32 2014 thomas nieto
*/

#ifndef MONITOR_FDS_H_
# define MONITOR_FDS_H_

# define WAIT_TO_HANDLE_CO ((struct timeval){1, 0})
# define WAIT_TO_RESP ((struct timeval){0, 0})
# define WAIT_TO_IDENTITY ((struct timeval){5, 0})
# define WAIT_TO_REQ ((struct timeval){0, 0})

int		select_read_fd(int, struct timeval);
int		select_write_fd(int, struct timeval);

int		select_read_fds(int *, int *, int, struct timeval);
int		select_write_fds(int *, int *, int, struct timeval);

#endif /* ! MONITOR_FDS_H_ */
