/*
** request.h for server in /home/abd-el_y/work/psu_2013_zappy/server/src
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Wed Jul  9 13:36:16 2014 Abd-el rahman
** Last update Sat Jul 12 18:59:12 2014 Abd-el rahman
*/

#ifndef REQUEST_H_
# define REQUEST_H_
# define INV_STR "{nourriture %d, linemate %d, deraumere %d, sibur %d,\
mendiane %d, phiras %d, thystame %d}\n"

# include <sys/select.h>

# include "ai_buffer.h"
# include "server_time.h"
# include "circular_buffer.h"
# include "data.h"
# include "server.h"

typedef struct	s_tab_incant
{
  int		id_incant;
  long long int	time_resp;
}		t_tab_incant;

typedef struct	s_incant
{
  t_quant	level;
  t_quant	linemate;
  t_quant	deraumere;
  t_quant	sibur;
  t_quant	mendiane;
  t_quant	phiras;
  t_quant	thystame;
  t_quant	nbplayer;
}		t_incant;

typedef struct	s_cmd
{
  t_request	request;
  t_delay	delay;
  int		(*func)(t_data *, t_player *, t_aidata *d);
}		t_cmd;

int		move_func(t_data *, t_player *, t_aidata *);
int		right_func(t_data *, t_player *, t_aidata *);
int		left_func(t_data *, t_player *, t_aidata *);
int		broadcast(t_data *, t_player *, t_aidata *);
int		invent_func(t_data *, t_player *, t_aidata *);
int		take_func(t_data *, t_player *, t_aidata *);
int		drop_func(t_data *, t_player *, t_aidata *);
int		see_func(t_data *, t_player *, t_aidata *);
int		connectt_func(t_data *, t_player *, t_aidata *);
int		incant_func(t_data *, t_player *, t_aidata *);
int		forke_func(t_data *, t_player *, t_aidata *);
int		expel_func(t_data *, t_player *, t_aidata *);
t_quant		find_item(t_item, t_citem);
void		add_item(t_item *, t_citem, int);
t_quant		find_item_incant(int, int);
int		init_incant();
void		check_incant();
int             check_res(t_player *);
int             incant_func(t_data *, t_player *, t_aidata *);
int             new_incant(t_player *player, t_data *d);
int		cplayer(t_quant, int *, t_player *);
int		do_req();

#endif /* !REQUEST_H_ */
