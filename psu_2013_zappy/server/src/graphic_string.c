/*
** graphic_string.c for graphic_string in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server/src
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Fri Jun 27 11:35:39 2014 Adrien Chataignoux
** Last update Sun Jul 13 14:13:33 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "server.h"
#include "graphic_com.h"

int		player_broadcast(int fd, int playernum, char *instruction)
{
  char		*str;
  int		len;
  char		*cut;

  len = strlen(instruction) + 10;
  if ((str = malloc(len * sizeof (char))) == NULL)
    return (4);
  if ((cut = extract_broadcast(instruction)) == NULL)
    return (4);
  sprintf(str, "pbc %d %s\n", playernum, cut);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_incant_end(int fd, int playernum, char *instruction)
{
  char		*str;
  int		flag;

  flag = 0;
  if ((str = malloc(25 * sizeof(char))) == NULL)
    return (4);
  if (instruction[5] == '1')
    flag = 1;
  sprintf(str, "pie %d %d %d\n", g_player[playernum].pos.x
	  , g_player[playernum].pos.y, flag);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_drop(int fd, int playernum, char *instruction)
{
  char		*str;
  int		ressource;

  ressource = instruction[5] - '0';
  if ((str = malloc(15 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "pdr %d %d\n", playernum, ressource);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_take(int fd, int playernum, char *instruction)
{
  char		*str;
  int		ressource;

  ressource = instruction[5] - '0';
  if ((str = malloc(15 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "pgt %d %d\n", playernum, ressource);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		egg_laid(int fd, int playernum, char *instruction)
{
  char		*str;
  int		eggnb;

  eggnb = atoi(instruction + 4);
  if ((str = malloc(25 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "enw %d %d %d %d", eggnb, playernum
	  , g_player[playernum].pos.x, g_player[playernum].pos.y);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}
