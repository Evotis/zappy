/*
** graphic_com2.c for graphic_com2 in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server/src
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Fri Jun 27 15:04:39 2014 Adrien Chataignoux
** Last update Thu Jul 10 07:29:57 2014 Abd-el rahman
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "server.h"
#include "graphic_com.h"

int		map_case(int fd, int playernum, char *instruction)
{
  char		*str;
  int		x;
  int		y;
  t_item	it;

  (void) playernum;
  if ((str = malloc(40 * sizeof(char))) == NULL)
    return (4);
  if (instruction[3] == '\n')
    return (map_content(fd, playernum));
  extract_pos(&x, &y, instruction);
  if (x > g_opt.x || y > g_opt.y)
    return (wrong_param(fd));
  it = g_map[y][x].item;
  sprintf(str, "bct %d %d %d %d %d %d %d %d %d\n", x, y, it.food, it.linemate,
	  it.deraumere, it.sibur, it.mendiane, it.phiras, it.thystame);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		map_size(int fd)
{
  char		*str;

  if ((str = malloc(16 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "msz %d %d\n", g_opt.x, g_opt.y);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		team_names(int fd)
{
  int		i;
  char		*str;
  int		len;

  i = 0;
  while (g_opt.team[i]->name)
    {
      len = strlen(g_opt.team[i]->name) + 5;
      if ((str = malloc((len + 1) * sizeof(char))) == NULL)
        return (4);
      sprintf(str, "tna %s\n", g_opt.team[i]->name);
      if (write(fd, str, strlen(str)) == -1)
        return (11);
      free(str);
      i++;
    }
  return (0);
}

int		wrong_param(int fd)
{
  if (write(fd, "spb\n", 4) == -1)
    return (11);
  return (0);
}

int		unknown_cmd(int fd)
{
  if (write(fd, "suc\n", 4) == -1)
    return (11);
  return (0);
}
