/*
** main.c for  in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by nieto_t
** Login   <thomas.nieto@epitech.net>
**
** Started on Sat Jun 21 15:02:30 2014 thomas nieto
** Last update Sun Jul 13 21:17:02 2014 Adrien Chataignoux
*/

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "main.h"
#include "request.h"
#include "server.h"

/*
** options data
*/
t_opt			g_opt;

/*
** heap allocated map's array
*/
t_map			**g_map = NULL;

/*
** graphic infos managed
** a part from others clients
*/
t_info			g_graphic_client;

/*
**item appearance
*/

long long int		g_delay_item[7];

/*
** server infos containing
** fds sets, sockfd,
*/
t_server		g_server;

/*
** circular buffer
** this, handles clients responses
*/
t_cbuffer		*g_resp_clients = NULL;

/*
** stack allocated player's array
** limited to the same backlog given as parameter to listen
** nb_connected allow to loop over g_player in a limited range
** given by the number of connected clients
*/
t_player		g_player[BACKLOG];
unsigned short		g_nb_connected = 0;

static const t_opt_tab	g_opt_tab[OPTNB] =
{
  {'p', get_port},
  {'x', get_x},
  {'y', get_y},
  {'n', get_team_name},
  {'t', get_time_unit},
  {'c', get_nb_client},
  {'d', debug}
};

static char		*g_default_team_name[3] =
{
  "team1",
  "team2",
  NULL
};

/*
** here are gathered all the default values given for options
*/
static int		init_opts(void)
{
  int			err;
  int			i;

  err = 0;
  g_opt.port = htons(3074);
  g_opt.x = 30;
  g_opt.y = 30;
  if ((g_opt.team = malloc(3 * sizeof(*g_opt.team))))
    for (i = 0; i < 3; ++i)
    {
      if ((g_opt.team[i] = malloc(sizeof(**g_opt.team))))
        g_opt.team[i]->name = g_default_team_name[i];
    }
  else
  {
    fprintf(stderr, "malloc failrure\n");
    err = 4;
  }
  g_opt.time_unit = 100;
  g_opt.debug = 0;
  g_opt.nb_client = 2;
  return (err);
}

/*
** routine executed as soon as the program is launched to get options
*/
static int		get_args(int argc, char *argv[])
{
  int			err;
  int			opt;
  int			i;

  err = 0;
  if (!(err = init_opts()))
    while (!err && (opt = getopt(argc, argv, "np:x:y:c:t:d")) != -1
        && opt != '?')
      for  (i = 0; !err && i < OPTNB; ++i)
        if (g_opt_tab[i].opt_char == opt)
          err = g_opt_tab[i].fct(optarg, argv);
  if (opt == '?')
    err = 1;
  for (i = 0; g_opt.team[i]->name; ++i)
    g_opt.team[i]->nb = g_opt.nb_client;
  return (err);
}

/*
** Once arguments are retrieved. This function will handle the
** server's initialization by creating socket, bind it and place it
** in passive mode in order to, later on, accept incoming connections.
** Also, it will initiate the graphic client's socket and the
** circular buffer to store clients's response
*/
static int		init_server(void)
{
  int			err;

  if (!(err = init_map())
      && !(err = init_players()) && !(err = init_incant()))
  {
    if (!(err = create_socket(&(g_server.info.sockfd))))
      if (!(err = bind_socket(&g_server.info)))
        if (!(err = listen_connection(g_server.info.sockfd)))
        {
          FD_ZERO(&g_server.readfds);
          FD_ZERO(&g_server.writefds);
        }
    g_graphic_client.sockfd = SOCK_OPEN;
    memset(&g_graphic_client.addr, 0, sizeof(g_graphic_client.addr));
    if ((g_resp_clients = cb_create(BACKLOG)) == NULL)
    {
      fprintf(stderr, "unable to init the circular buffer\n");
      err = 4;
    }
  }
  init_delay_item();
  return (err);
}

int			main(int argc, char *argv[])
{
  int			err;

  err = 0;
  if (!(err = get_args(argc, argv))
      && !(err = init_server()))
  {
    if (DEBUG)
    {
      print_args();
      print_infos(&g_server.info);
    }
    err = routine();
    (void)close_socket(g_server.info.sockfd);
    free(g_opt.team);
    cb_free(g_resp_clients);
    player_free();
  }
  return (err);
}
