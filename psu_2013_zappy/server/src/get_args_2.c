/*
** get_args_2.c for  in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by thomas nieto
** Login   <n0t@epitech.net>
**
** Started on Sun Jun 22 02:54:08 2014 thomas nieto
** Last update Mon Jun 23 18:18:28 2014 thomas nieto
*/

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

#include "main.h"

int			get_nb_client(char *nb_client,
				      __attribute__((unused))char *argv[])
{
  int			err;
  unsigned short	tmp;

  err = 0;
  tmp = strtol(nb_client, NULL, 10);
  if (errno == ERANGE)
    {
      fprintf(stderr, "invalid number of clients. Exiting.\n");
      err = 6;
    }
  else
    g_opt.nb_client = tmp;
  return (err);
}

int			debug(__attribute__((unused))char *optarg,
			      __attribute__((unused))char *argv[])
{
  g_opt.debug = 1;
  return (0);
}
