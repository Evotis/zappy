/*
** server_debug.c for  in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by thomas nieto
** Login   <n0t@epitech.net>
**
** Started on Sun Jun 22 00:59:28 2014 thomas nieto
** Last update Sun Jul 13 12:17:21 2014 Adrien Chataignoux
*/

#include <stdio.h>

#include "server.h"

void		print_args(void)
{
  t_team       	**tmp;

  (void)fprintf(stderr, " \
     port: %u\n \
        x: %u\n \
        y: %u\n \
time_unit: %u\n \
team_name: \n\n",
		htons(g_opt.port),
		g_opt.x,
		g_opt.y,
		g_opt.time_unit);
  tmp = g_opt.team;
  while (tmp && *tmp && (*tmp)->name)
    {
      (void)fprintf(stderr, "'%s' for [%d] players\n",
		    (*tmp)->name, (*tmp)->nb);
      tmp += 1;
    }
  puts("\n");
}

void		print_infos(t_info *info)
{
  fprintf(stderr, "\
      new open socket - sockfd: %d\n", info->sockfd);
}

