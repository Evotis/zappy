/*
** cmd.c for server in /home/abd-el_y/work/psu_2013_zappy/server/src
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Wed Jul  9 15:42:11 2014 Abd-el rahman
** Last update Sun Jul 13 22:07:23 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdio.h>

#include "request.h"
#include "see.h"

inline t_quant	find_item(t_item inv, t_citem item)
{
  return (*((t_quant *)&inv + (int)item));
}

void	add_item(t_item *inv, t_citem item, int nb)
{
  *(((t_quant *)(inv)) + item) += nb;
}

int	take_func(t_data *d, t_player *player, t_aidata *aid)
{
  if (find_item(g_map[player->pos.y][player->pos.x].item, aid->citem) > 0)
    {
      add_item(&g_map[player->pos.y][player->pos.x].item, aid->citem, -1);
      add_item(&player->bag, aid->citem, 1);
      sprintf(d->resp[0], "ok\n");
      sprintf(d->resp[1], "pgt %d\n", aid->citem);
    }
  else
    sprintf(d->resp[0], "ko\n");
  return (0);
}

int	drop_func(t_data *d, t_player *player, t_aidata *aid)
{
  if (find_item(player->bag, aid->citem) > 0)
    {
      add_item(&g_map[player->pos.y][player->pos.x].item, aid->citem, 1);
      add_item(&player->bag, aid->citem, -1);
      sprintf(d->resp[0], "ok\n");
      sprintf(d->resp[1], "pdr %d\n", aid->citem);
    }
  else
    sprintf(d->resp[0], "ko\n");
  return (0);
}

int	invent_func(t_data *d, t_player *player,
		    __attribute__((unused))t_aidata *aid)
{
  sprintf(d->resp[0], INV_STR,
	  find_item(player->bag, FOOD),
	  find_item(player->bag, LINEMATE), find_item(player->bag, DERAUMERE),
	  find_item(player->bag, SIBUR), find_item(player->bag, MENDIANE),
	  find_item(player->bag, PHIRAS), find_item(player->bag, THYSTAME));
  sprintf(d->resp[1], "pin\n");
  return (0);
}
