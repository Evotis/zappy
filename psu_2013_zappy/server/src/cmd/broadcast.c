/*
** broadcast.c for zappy in /home/abd-el_y/work/zappy
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Sun Jun 29 19:11:37 2014 Abd-el rahman
** Last update Fri Jul 11 23:31:26 2014 Abd-el rahman
*/

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "data.h"
#include "server.h"
#include "broadcast.h"

static const t_ptr_vec	g_tab_vec[4] =
  {
    {NORTH, 0, 1},
    {EAST, 1, 0},
    {SOUTH, 0, -1},
    {WEST, -1, 0}
  };

float	getAngle(t_vec *unit, t_vec *dir)
{
  float	angle;

  angle = acosf((unit->x * dir->x + unit->y * dir->y) /
		(sqrtf(pow(dir->x, 2) + pow(dir->y, 2))));
  if ((unit->x * dir->y - unit->y * dir->x) < 0)
    return (-angle);
  return (angle);
}

void	projection(t_pos *dest, t_pos *src, int diff_x, int diff_y)
{
  if (diff_y >= diff_x)
    {
      if (dest->y < src->y)
	dest->y += g_opt.y;
      else
	src->y += g_opt.y;
    }
  else
    {
      if (dest->x < src->x)
	dest->x += g_opt.x;
      else
	src->x += g_opt.x;
    }
}

t_vec		getVector(t_pos *dest, t_pos *src)
{
  t_vec		ret;
  unsigned int	diff_x;
  unsigned int	diff_y;

  if (dest->y - src->y >= 0)
    diff_y = dest->y - src->y;
  else
    diff_y = src->y - dest->y;
  if (dest->x - src->x >= 0)
    diff_x = dest->x - src->x;
  else
    diff_x = src->x - dest->x;
  if (diff_y > g_opt.y / 2 || diff_x > g_opt.x / 2)
    projection(dest, src, diff_x, diff_y);
  ret.x = src->x - dest->x;
  ret.y = dest->y - src->y;
  return (ret);
}

int		send_broadcast(t_player *src, t_player *dest,
			       t_data *tmp, t_aidata *aid)
{
  int		i;
  t_vec		unit;
  t_vec		direction;
  float		angle;

  i = 0;
  if (src->pos.x == dest->pos.x && src->pos.y == dest->pos.y)
    sprintf(tmp->resp[0], "message 0,%s\n", aid->broadcast);
  else
    {
      while (i < 4)
	if (g_tab_vec[i++].dir == dest->direction)
	  {
	    unit.x = g_tab_vec[i].x;
	    unit.y = g_tab_vec[i].y;
	  }
      direction = getVector(&dest->pos, &src->pos);
      angle = getAngle(&unit, &direction);
      angle = angle < 0 ? (angle + 2 * M_PI) : angle;
      sprintf(tmp->resp[0], "message %d,%s\n", (int)POS_BROADCAST(angle),
	      aid->broadcast);
    }
  sprintf(tmp->resp[1], "ok\n");
  return (0);
}

int		broadcast(t_data *d, t_player *player, t_aidata *aid)
{
  int		i;
  t_data	tmp;

  i = 0;
  while (i < g_nb_connected)
    {
      if (g_player[i].info.sockfd != player->info.sockfd)
	{
	  memset(&tmp, 0, sizeof(tmp));
	  tmp.sockfd = g_player[i].info.sockfd;
	  tmp.resp_time = get_resp_time(BROADCAST);
	  send_broadcast(player, &g_player[i], &tmp, aid);
	  cb_push(g_resp_clients, &tmp);
	}
      ++i;
    }
  sprintf(d->resp[0], "ok\n");
  sprintf(d->resp[1], "pbc\n");
  return (0);
}
