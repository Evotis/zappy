/*
** incant2.c for incant2 in /home/fest/project/sysu/zappy/PSU_2013_zappy/server/src/cmd
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Sun Jul 13 03:57:34 2014 Adrien Chataignoux
** Last update Sun Jul 13 12:14:24 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdio.h>

#include "request.h"
#include "server.h"

extern t_incant		g_incant[7];
extern t_tab_incant	g_tab_in[100];

static inline void	end_incant_lines(int x, t_data tmp, int i, int flag)
{
  if (flag == 0 && check_res(&(g_player[x])) == 0 &&
      cplayer(g_incant[g_player[x].level - 1].nbplayer
	      , &flag, &(g_player[x])))
    {
      sprintf(tmp.resp[0], "niveau actuel %d\n", g_player[x].level);
      g_player[x].level += 1;
    }
  else
    sprintf(tmp.resp[0], "ko");
  g_player[x].id_incant = -1;
  tmp.resp_time = NOW;
  cb_push(g_resp_clients, &tmp);
  g_tab_in[i].id_incant = -1;
}

static int		end_incant(int i)
{
  int		x;
  t_data	tmp;
  int		flag;

  flag = 0;
  for (x = 0; x < g_nb_connected; x++)
    {
      memset(&tmp, 0, sizeof(tmp));
      if (g_player[x].id_incant == g_tab_in[i].id_incant)
	end_incant_lines(x, tmp, i, flag);
    }
  return (flag);
}

void		check_incant()
{
  int		i;
  t_data	tmp;

  i = 0;
  while (i < 100)
    {
      if (g_tab_in[i].time_resp <= get_current_time()
	  && g_tab_in[i].id_incant != -1)
	{
	  if (end_incant(i) == 0)
	    sprintf(tmp.resp[1], "pie 1\n");
	  else
	    sprintf(tmp.resp[1], "pie 0\n");
	  cb_push(g_resp_clients, &tmp);
	}
      i++;
    }
}

int		new_incant(t_player *player, t_data *d)
{
  int		i;

  while (i < 100 && g_tab_in[i].id_incant != -1)
    i++;
  if (i == 99)
    return (-1);
  g_tab_in[i].id_incant = i;
  g_tab_in[i].time_resp = get_resp_time(300);
  player->id_incant = i;
  sprintf(d->resp[1], "pic %d\n", player->level);
  sprintf(d->resp[0], "elevation en cours");
  d->resp_time = NOW;
  return (0);
}
