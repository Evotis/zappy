/*
** see.c for server in /home/abd-el_y/rendu/psu_2013_zappy/server
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Thu Jul 10 11:48:54 2014 Abd-el rahman
** Last update Sun Jul 13 11:47:49 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdio.h>

#include "request.h"
#include "see.h"

static const t_word	g_word[7] =
  {
    {FOOD, " nourriture"},
    {LINEMATE, " linemate"},
    {DERAUMERE, " deraumere"},
    {SIBUR, " sibur"},
    {MENDIANE, " mendiane"},
    {PHIRAS, " phiras"},
    {THYSTAME, " thystame"},
  };

void	set_cone(t_player *player, int *caise, int *line)
{
  if (player->direction == NORTH)
    {
      *caise = player->pos.x - *line + *caise;
      *line = player->pos.y - *line;
    }
  else if (player->direction == SOUTH)
    {
      *caise = player->pos.x + *line - *caise;
      *line = player->pos.y + *line;
    }
  else if (player->direction == EAST)
    {
      *caise = player->pos.x + *line;
      *line = player->pos.y - *line + *caise;
    }
  else
    {
      *caise = player->pos.x - *line;
      *line = player->pos.y + *line - *caise;
    }
}

void	limit(int *caise, int *line)
{
  if (*line > g_opt.y)
    *line = *line - g_opt.y;
  else if (*line < 0)
    *line = g_opt.y + *line;
  if (*caise > g_opt.x)
    *caise = *caise - g_opt.x;
  else if (*caise < 0)
    *caise = g_opt.x + *caise;
}

void		find_case(t_data *d, t_player *player, int caise, int line)
{
  int		i;
  int		tmp;
  int		citem;

  i = -1;
  citem = FOOD;
  set_cone(player, &caise, &line);
  limit(&caise, &line);
  while (++i < g_nb_connected)
    if (g_player[i].pos.x == caise && g_player[i].pos.y == line)
      strcat((char *)d->resp[0], " joueur");
  while (citem < DEFAUT)
    {
      tmp = find_item(g_map[line][caise].item, citem);
      i = 0;
      while (i < tmp)
	{
	  strcat(d->resp[0], g_word[citem].str);
	  ++i;
	}
      ++citem;
    }
  strcat(d->resp[0], ",");
}

int	see_func(t_data *d, t_player *player,
		 __attribute__((unused))t_aidata *aid)
{
  int	i;
  int	line;
  int	caise;

  line = 0;
  caise = 1;
  strcat(d->resp[0], "{");
  while (line <= (int)player->level)
    {
      i = 0;
      while (i < caise)
	{
	  find_case(d, player, i, line);
	  ++i;
	}
      ++line;
      caise += 2;
    }
  d->resp[0][strlen(d->resp[0]) - 1] = 0;
  strcat(d->resp[0], "}\n");
  return (0);
}
