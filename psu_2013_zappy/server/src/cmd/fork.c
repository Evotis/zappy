/*
** fork.c for fork in /home/fest/project/sysu/zappy/PSU_2013_zappy/server/src/cmd
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Sat Jul 12 23:08:23 2014 Adrien Chataignoux
** Last update Sun Jul 13 11:46:41 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdio.h>
#include "ai_buffer.h"
#include "server.h"

int	connectt_func(t_data *d, t_player *player,
		   __attribute__((unused))t_aidata *aid)
{
  int	nb;
  int	i;

  i = 0;
  nb = -1;
  while (g_opt.team[i] != NULL)
    i++;
  if (!strcmp(g_opt.team[i]->name, player->team_name))
    nb = g_opt.team[i]->nb;
  sprintf(d->resp[0], "%d\n", nb);
  return (0);
}

int	forke_func(t_data *d, t_player *player,
		   __attribute__((unused))t_aidata *aid)
{
  int	i;

  i = 0;
  g_player[g_nb_connected].info.sockfd = SOCK_OPEN;
  g_player[g_nb_connected].pos.x = player->pos.x;
  g_player[g_nb_connected].pos.y = player->pos.y;
  while (g_opt.team[i] != NULL)
    {
      if (!strcmp(g_opt.team[i]->name, player->team_name))
	  g_opt.team[i]->nb += 1;
      i++;
    }
  sprintf(d->resp[0], "ok\n");
  sprintf(d->resp[1], "enw\n");
  return (0);
}
