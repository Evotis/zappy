/*
** request.c for server in /home/abd-el_y/work/psu_2013_zappy/server/src
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Wed Jul  9 13:36:23 2014 Abd-el rahman
** Last update Sun Jul 13 23:10:30 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdio.h>

#include "main.h"
#include "request.h"

static const t_cmd	g_cmd[12] =
  {
    {MOVE, AVANCE, &move_func},
    {RIGHT, DROITE, &right_func},
    {LEFT, GAUCHE, &left_func},
    {SEE, 7, &see_func},
    {INVENT, 1, &invent_func},
    {TAKE, 7, &take_func},
    {DROP, 7, &drop_func},
    {EXPEL, 7, &expel_func},
    {BROAD, 7, &broadcast},
    {INCANT, 300, &incant_func},
    {FORKE, 42, &forke_func},
    {CONNECTT, 7, &connectt_func}
  };

static void	move_expel(t_player *player, int i)
{
  if (player->direction == NORTH)
    g_player[i].pos.y = ((g_player[i].pos.y == 0)
			 ? g_opt.y : g_player[i].pos.y - 1);
  else if (player->direction == SOUTH)
    g_player[i].pos.y = ((g_player[i].pos.y == g_opt.y)
			 ? 0 : g_player[i].pos.y + 1);
  else if (player->direction == EAST)
    g_player[i].pos.x = ((g_player[i].pos.x == g_opt.x)
			 ? 0 : g_player[i].pos.x + 1);
  else
    g_player[i].pos.x = ((g_player[i].pos.x == 0)
			 ? g_opt.x : g_player[i].pos.x - 1);
}

static void	fulfill_client(t_player *pl, t_data *tmp)
{
  if (pl->direction == NORTH)
    sprintf(tmp->resp[0], "deplacement: 3\n");
  else if (pl->direction == SOUTH)
    sprintf(tmp->resp[0], "deplacement: 1\n");
  else if (pl->direction == EAST)
    sprintf(tmp->resp[0], "deplacement:4\n");
  else
    sprintf(tmp->resp[0], "deplacement: 2\n");
}

int		expel_func(t_data *d, t_player *pl,
			   __attribute__((unused))t_aidata *aid)
{
  int		i;
  t_data	tmp;

  i = 0;
  for (i = 0; i < g_nb_connected; i++)
    {
      memset(&tmp, 0, sizeof(tmp));
      if (g_player[i].pos.x == pl->pos.x && g_player[i].pos.y == pl->pos.y)
	{
	  move_expel(pl, i);
	  fulfill_client(pl, &tmp);
	  cb_push(g_resp_clients, &tmp);
	}
    }
  sprintf(d->resp[0], "ok\n");
  sprintf(d->resp[1], "pex %d\n", pl->num);
  return (0);
}

static int		request(t_aidata *d, t_player *player)
{
  int		i;
  t_data	tmp;

  i = 0;
  memset(&tmp, 0, sizeof(tmp));
  tmp.sockfd = player->info.sockfd;
  while (i < 8)
    {
      if (d->request == g_cmd[i].request)
	{
	  if (d->request != INCANT)
	    tmp.resp_time = get_resp_time(g_cmd[i].delay);
	  if (g_cmd[i].func(&tmp, player, d) == -1)
	    return (-1);
	}
      ++i;
    }
  d->request = DEFAULT;
  cb_push(g_resp_clients, &tmp);
  return (0);
}

int		do_req()
{
  int		i;
  t_aidata	*tmp;

  for (i = 0; i < g_nb_connected; ++i)
    {
      if ((tmp = ai_read(g_player[i].ai_bc)) != NULL)
	{
	  if (DEBUG)
	    printf("do_req\n");
	  if ((request(tmp, &g_player[i])) == -1)
	    return (-1);
	}
    }
  return (0);
}
