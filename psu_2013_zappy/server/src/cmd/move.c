/*
** move.c for server in /home/abd-el_y/rendu/psu_2013_zappy/server
** 
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
** 
** Started on  Thu Jul 10 11:51:44 2014 Abd-el rahman
** Last update Sun Jul 13 18:33:35 2014 Adrien Chataignoux
*/

#include <stdio.h>

#include "request.h"

int	move_func(t_data *d, t_player *player,
		  __attribute__((unused))t_aidata *aid)
{
  if (player->direction == NORTH)
    player->pos.y = ((player->pos.y == 0) ? g_opt.y : player->pos.y - 1);
  else if (player->direction == SOUTH)
    player->pos.y = ((player->pos.y == g_opt.y) ? 0 : player->pos.y + 1);
  else if (player->direction == EAST)
    player->pos.x = ((player->pos.x == g_opt.x) ? 0 : player->pos.x + 1);
  else
    player->pos.x = ((player->pos.x == 0) ? g_opt.x : player->pos.x - 1);
  sprintf(d->resp[0], "ok\n");
  sprintf(d->resp[1], "ppo\n");
  return (0);
}

int	right_func(t_data *d, t_player *player,
		   __attribute__((unused))t_aidata *aid)
{
  if (player->direction == WEST)
    player->direction = NORTH;
  else
    player->direction += 1;
  sprintf(d->resp[0], "ok\n");
  sprintf(d->resp[1], "ppo\n");
  return (0);
}

int	left_func(t_data *d, t_player *player,
		  __attribute__((unused))t_aidata *aid)
{
  if (player->direction == NORTH)
    player->direction = WEST;
  else
    player->direction -= 1;
  sprintf(d->resp[0], "ok\n");
  sprintf(d->resp[1], "ppo\n");
  return (0);
}
