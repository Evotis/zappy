/*
** incant.c for incant in /home/fest/project/sysu/zappy/PSU_2013_zappy/server/src/cmd
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Sat Jul 12 21:49:40 2014 Adrien Chataignoux
** Last update Sun Jul 13 12:10:45 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdio.h>
#include "request.h"

t_incant	g_incant[7] =
  {
    {1, 1, 0, 0, 0, 0, 0, 1},
    {2, 1, 1, 1, 0, 0, 0, 2},
    {3, 2, 0, 1, 0, 2, 0, 2},
    {4, 1, 1, 2, 0, 1, 0, 4},
    {5, 1, 2, 1, 3, 0, 0, 4},
    {6, 1, 2, 3, 0, 1, 0, 6},
    {7, 2, 2, 2, 2, 2, 1, 6}
  };

t_tab_incant	g_tab_in[100];

int		init_incant()
{
  int		i;

  i = 0;
  while (i < 100)
    {
      g_tab_in[i].id_incant = -1;
      i++;
    }
  return (0);
}

int		cplayer(t_quant nb, int *flag, t_player *player)
{
  int		i;
  int		count;
  int		x;
  int		y;
  int		id;

  id = -1;
  i = 0;
  x = player->pos.x;
  y = player->pos.y;
  while (i < g_nb_connected)
    {
      if (g_player[i].pos.x == x && g_player[i].pos.y == y
	  && g_player[i].level == player->level)
	{
	  if (id == -1)
	    id = g_player[i].id_incant;
	  ++count;
	}
      ++i;
    }
  if (count < nb)
    *flag = -1;
  return (id);
}

int		check_res(t_player *player)
{
  int		flag;
  int		i;

  i = 0;
  flag = 0;
  while (i < 6)
    {
      if (find_item(g_map[player->pos.y][player->pos.x].item, i + 1) <
	  find_item_incant(i + 1, player->level - 1))
	flag = -1;
    }
  return (flag);
}

inline t_quant	find_item_incant(int it, int level)
{
  return (*((t_quant *)&(g_incant[level]) + it));
}

int		incant_func(t_data *d, t_player *player,
			     __attribute__((unused)) t_aidata *aid)
{
  int		flag;
  int		id;

  flag = 0;
  if ((id = cplayer(g_incant[player->level - 1].nbplayer, &flag, player))
      != -1 && flag == 0 && check_res(player) == 0)
    new_incant(player, d);
  else if (id != -1 && flag == 0 && check_res(player) == 0)
    {
      sprintf(d->resp[0], "elevation en cours\n");
      d->resp_time = NOW;
      player->id_incant = id;
    }
  else
    {
      sprintf(d->resp[0], "ko\n");
      d->resp_time = NOW;
    }
  return (0);
}
