/*
** data.c for zappy_server in /home/abd-el_y/work/zappy
**
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
**
** Started on  Sun Jun 22 15:34:34 2014 Abd-el rahman
** Last update Sat Jul 12 15:09:47 2014 Abd-el rahman
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "server.h"
#include "server_time.h"
#include "data.h"

void		init_item(char type, t_item *item)
{
  if (type == PLAYER)
    item->food = 10;
  else
    item->food = 0;
  item->linemate = 0;
  item->deraumere = 0;
  item->sibur = 0;
  item->mendiane = 0;
  item->phiras = 0;
  item->thystame = 0;
}

/*
** each cases of the map is initialize with 0 at each item fields
** later on, theses ones will be set with random values
*/
int		init_map(void)
{
  int		i;
  int		j;

  i = 0;
  if ((g_map = malloc(sizeof(*g_map) * g_opt.y)) == NULL)
    {
      fprintf(stderr, "Malloc failure - can't build map. Exiting\n");
      return (4);
    }
  while (i < g_opt.y)
    {
      j = 0;
      if ((g_map[i] = malloc(sizeof(**g_map) * g_opt.x)) == NULL)
	{
	  fprintf(stderr, "Malloc failure - can't build map. Exiting\n");
	  return (4);
	}
      while (j < g_opt.x)
	{
	  init_item(MAP, (&g_map[i][j].item));
	  ++j;
	}
      ++i;
    }
  return (0);
}

/*
** be ready for battle:
** when a slot is found
** datas are stored according to informations got by the server
*/
int			init_player(t_info *info, int pos, int pos_team_info)
{
  int			err;

  err = 0;
  if (g_opt.team[pos_team_info]->nb)
  {
    g_player[pos].team_name = g_opt.team[pos_team_info]->name;
    (void)memcpy(&g_player[pos].info, info, sizeof(*info));
    g_opt.team[pos_team_info]->nb -= 1;
    if (g_nb_connected)
      g_player[pos].num = g_player[g_nb_connected - 1].num + 1;
    else
      g_player[pos].num = 1;
    g_nb_connected += 1;
  }
  else
    err = 1;
  return (err);
}

/*
** will initialize the player array
**
** the n firsts fds will be set to SOCK_OPEN
** when n > nb_client fds are set to SOCK_CLOSE until an egg is laid
*/
int			init_players(void)
{
  unsigned short	i;
  unsigned short	nb_to_connect;

  nb_to_connect = 0;
  for (i = 0; g_opt.team[i] && g_opt.team[i]->name; ++i)
      nb_to_connect += g_opt.team[i]->nb;
  for (i = 0; i < BACKLOG; ++i)
  {
    g_player[i].pos.x = 0;
    g_player[i].pos.y = 0;
    g_player[i].num = 0;
    (void)memset(&(g_player[i].info.addr), 0, sizeof(g_player[i].info.addr));
    ((i < nb_to_connect) ? (g_player[i].info.sockfd = SOCK_OPEN)
     : (g_player[i].info.sockfd = SOCK_CLOSE));
    g_player[i].id_incant = -1;
    g_player[i].level = 1;
    g_player[i].ai_bc = ai_create(10);
    g_player[i].life = 1;
    g_player[i].team_name = NULL;
    g_player[i].direction = EAST;
    g_player[i].need_eat = get_resp_time(EAT);
    init_item(PLAYER, &(g_player[i].bag));
  }
  return (0);
}

int		init_one_player(int pos)
{
  if ((g_player[pos].ai_bc = ai_create(10)) == NULL)
    return (-1);
  g_player[pos].pos.x = 0;
  g_player[pos].pos.y = 0;
  (void)memset(&(g_player[pos].info.addr), 0, sizeof(g_player[pos].info.addr));
  g_player[pos].info.sockfd = SOCK_CLOSE;
  g_player[pos].level = 1;
  g_player[pos].life = 1;
  g_player[pos].team_name = NULL;
  g_player[pos].direction = EAST;
  g_player[pos].need_eat = get_resp_time(EAT);
  init_item(PLAYER, &(g_player[pos].bag));
  return (0);
}
