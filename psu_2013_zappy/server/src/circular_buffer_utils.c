/*
** circular_buffer_utils.h for include in /home/n0t/dev/circular_buffer/include
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Tue Jul 01 16:38:33 2014 thomas nieto
** Last update Fri Jul 11 20:45:43 2014 Abd-el rahman
*/

#include <stdlib.h>
#include <stdio.h>

#include "server.h"
#include "circular_buffer.h"

void		cb_clear(int fd)
{
  size_t	i;

  for (i = 0; i < g_resp_clients->len; ++i)
    if (g_resp_clients->data[i]->sockfd == fd)
      g_resp_clients->data[i]->sockfd = NOP;
}

inline int		is_empty(const char * const str)
{
  return (str[0] == '\0');
}

int		cb_is_full(t_cbuffer *cb)
{
  size_t	i;

  for (i = 0; i < cb->len; ++i)
    if (is_empty(cb->data[i]->resp[0])
        && is_empty(cb->data[i]->resp[1]))
      return (!CB_FULL);
  return (CB_FULL);
}

void		cb_dump(t_cbuffer *cb)
{
  size_t	i;

  fprintf(stderr, "r: %lu - w: %lu\n", cb->r, cb->w);
  for (i = 0; i < cb->len; ++i)
    fprintf(stderr, "[%zu] - sockfd: %d, resp_time: %lld, \
client_resp: %s graphic_resp: %s\n", i, cb->data[i]->sockfd,
        cb->data[i]->resp_time, cb->data[i]->resp[0], cb->data[i]->resp[1]);
}
