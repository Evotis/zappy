/*
** client_request.c for client_request in /home/fest/project/sysu/zappy/PSU_2013_zappy/server/src
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Mon Jul  7 14:21:18 2014 Adrien Chataignoux
** Last update Tue Jul  8 17:29:41 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <string.h>
#include "server.h"
#include "player_management.h"

int		read_client(int fd)
{
  FILE		*readfd;
  char		*lineptr;
  size_t	len;
  int		playernum;

  if ((playernum = find_player_from_fd(fd)) == -1)
    return (16);
  len = 0;
  if ((readfd = fdopen(g_player[playernum].info.sockfd, "r")) == NULL)
      return (16);
  if (getline(&lineptr, &len, readfd) == -1)
      return (17);
  if (strncmp(lineptr, "broadcast", 9) == 0)
    ai_push_broadcast(g_player[playernum].ai_bc, lineptr);
  else if (strncmp(lineptr, "prend", 5) == 0
	   || strncmp(lineptr, "pose", 4) == 0)
    ai_push_special(g_player[playernum].ai_bc, lineptr);
  else
    ai_push(g_player[playernum].ai_bc, lineptr);
  return (0);
}
