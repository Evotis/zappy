/*
** debug_2.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Tue Jun 24 15:40:05 2014 thomas nieto
** Last update Sun Jul 13 11:39:42 2014 Adrien Chataignoux
*/

#include <stdio.h>

#include "server.h"

static void	print_item(t_item *item)
{
  (void)fprintf(stderr, "\titems = f: %u - l: %u - d: %u - s: %u - \
m: %u - p: %u t: %u\n",
		item->food,
		item->linemate,
		item->deraumere,
		item->sibur,
		item->mendiane,
		item->phiras,
		item->thystame);
}

void		print_map(void)
{
  int		i;
  int		j;

  (void)fprintf(stderr, "map status\n");
  for (i = 0; i < g_opt.y; ++i)
    {
      for (j = 0; j < g_opt.x; ++j)
	{
	  (void)fprintf(stderr, "%d: {%d - %d} ", i,
			j,
			i);
	  print_item(&(g_map[i][j].item));
	}
    }
}

void		print_players(void)
{
  int		i;

  (void)fprintf(stderr, "player on board\n");
  for (i = 0; i < BACKLOG; ++i)
  {
    (void)fprintf(stderr, "%d: {%u - %u} fd -> %d", i,
        g_player[i].pos.x,
        g_player[i].pos.y,
        g_player[i].info.sockfd);

    print_item(&(g_player[i].bag));
  }
}

void		print_player(int nb)
{
  (void)fprintf(stderr, "player n°%d: {%u - %u} ", nb,
		g_player[nb].pos.x,
		g_player[nb].pos.y);
  print_item(&(g_player[nb].bag));
}
