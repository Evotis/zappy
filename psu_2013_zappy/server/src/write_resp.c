/*
** write_resp.c for PSU_2013_zappy in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sat Jul 12 18:52:12 2014 thomas nieto
** Last update Sat Jul 12 15:07:24 2014 Abd-el rahman
*/

#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "player_management.h"
#include "server.h"
#include "graphic_com.h"
#include "circular_buffer.h"
#include "monitor_fds.h"
#include "main.h"
#include "server_time.h"

/*
** if player, it writes on socket
** if graphic client, it send the action then write it
*/
static int		write_action(t_data *tmp, int type)
{
  int			err;

  err = 0;
  if (type == RESP_PLAYER_POS && !is_empty(tmp->resp[RESP_PLAYER_POS]))
    err = write(tmp->sockfd, tmp->resp[RESP_PLAYER_POS],
        strlen(tmp->resp[RESP_PLAYER_POS]));
  if (!is_empty(tmp->resp[RESP_GRAPHIC_POS]))
    err = launch_actions(tmp->resp[RESP_GRAPHIC_POS]
        , find_player_from_fd(tmp->sockfd));
  tmp->read = READ;
  return (err);
}

/*
** here, every fds where data need to be sended
** will be checked with select or destroy because
** they every fds here, should be available for writing
*/
static int	check_for_write(t_data *t)
{
  int		err;
  int		fds[2];
  int		fds_selected[2];
  int		i;

  err = 0;
  fds[RESP_PLAYER_POS] = t->sockfd;
  fds[RESP_GRAPHIC_POS] = g_graphic_client.sockfd;
  if (!t->read && select_write_fds(fds, fds_selected, 2, WAIT_TO_RESP) > 0)
    for (i = 0; i < 2; ++i)
      if (fds[i] > 0 && !fds_selected[i])
      {
        if (DEBUG)
          fprintf(stderr, "socket %d not ready for writing \
              - closing connection\n", fds[i]);
        death_player(fds[i]);
      }
      else if (fds[i] > 0 && fds_selected[i])
      {
        if (DEBUG)
          fprintf(stderr, "writing '%s' to socket: %d\n", t->resp[i], fds[i]);
        write_action(t, i);
      }
  return (err);
}

/*
** will get every slot not null in g_resp_client
** when it's time to write them
*/
void			write_resp(void)
{
  t_data		*tmp;
  size_t		i;

  for (i = 0; i < g_resp_clients->len; ++i)
    if ((tmp = cb_get(g_resp_clients, i)))
      if ((tmp->resp_time == NOW || tmp->resp_time >= get_current_time()))
        (void)check_for_write(tmp);
}
