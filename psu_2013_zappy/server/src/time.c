/*
** time.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Jul 07 15:32:47 2014 thomas nieto
** Last update Mon Jul  7 19:14:30 2014 Adrien Chataignoux
*/

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

static inline long long int	sec_to_u(struct timeval *time)
{
  return (time->tv_sec * 1000000 + time->tv_usec);
}

long long int		get_resp_time(int action)
{
  struct timeval	time;
  long long int		ret;

  gettimeofday(&time, NULL);
  ret = sec_to_u(&time);
  ret += (long long int)(((float)action / 100.0) * 1000000.0);
  return (ret);
}

long long int		get_current_time(void)
{
  struct timeval	time;

  gettimeofday(&time, NULL);
  return (sec_to_u(&time));
}
