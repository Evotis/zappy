/*
** graphic_actions.c for graphic_actions in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server/src
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Thu Jun 26 16:41:19 2014 Adrien Chataignoux
** Last update Sun Jul 13 14:19:31 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "server.h"
#include "graphic_com.h"

int		new_player(int fd, int playernum)
{
  char		*str;
  int		len;

  len = strlen(g_player[playernum].team_name) + 30;
  if ((str = malloc((len + 1) * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "pnw %d %d %d %c %d %s\n", playernum, g_player[playernum].pos.x
	  , g_player[playernum].pos.y, g_player[playernum].direction
	  , g_player[playernum].level, g_player[playernum].team_name);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_pos(int fd, int playernum)
{
  char		*str;

  if ((str = malloc(16 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "ppo %d %d %d %d\n", playernum, g_player[playernum].pos.x
          , g_player[playernum].pos.y, g_player[playernum].direction);
  if (write(fd, str, strlen(str)))
    return (11);
  free(str);
  return (0);
}

int		player_level(int fd, int playernum, char *instruction)
{
  char		*str;

  (void) instruction;
  if ((str = malloc(26 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "plv %d %d\n", playernum, g_player[playernum].level);
  if (write(fd, str, strlen(str)))
    return (11);
  free(str);
  return (0);
}

int		expulsion(int fd, int playernum)
{
  char		*str;

  if ((str = malloc(10 * sizeof (char))) == NULL)
    return (4);
  sprintf(str, "pex %d\n", playernum);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_starvation(int fd, int playernum)
{
  char		*str;

  if ((str = malloc(10 * sizeof(char))) == NULL)
    return (11);
  sprintf(str, "pdi %d\n", playernum);
  if (write(fd, str, strlen(str)) == -1)
    return (4);
  free(str);
  return (0);
}
