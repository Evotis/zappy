/*
** socket_mgmt.c for  in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by thomas nieto
** Login   <thomas.nieto@epitech.net>
**
** Started on Sun Apr 13 22:24:56 2014 thomas nieto
** Last update Thu Jul 10 20:02:21 2014 Abd-el rahman
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "server.h"
#include "main.h"

int		close_socket(int sockfd)
{
  int		err;

  err = 0;
  if ((close(sockfd)) == -1)
    {
      (void)fprintf(stderr, "Close sockfd failed\n");
      err = 255;
    }
  else
    (void)fprintf(stderr, "Server exiting.\n");
  return (err);
}

int		listen_connection(int sockfd)
{
  int		err;

  err = 0;
  if ((listen(sockfd, BACKLOG)) == -1)
    {
      (void)fprintf(stderr, "listen_client - connection: %s\n",
          strerror(errno));
      err = 10;
    }
  return (err);
}

int		bind_socket(t_info *info)
{
  int		err;
  socklen_t	addrlen;

  err = 0;
  info->addr.sin_family = DOMAINE;
  info->addr.sin_addr.s_addr = INADDR_ANY;
  info->addr.sin_port = g_opt.port;
  addrlen = sizeof(info->addr);
  if ((bind(info->sockfd, (struct sockaddr *)(&(info->addr)), addrlen)) == -1)
    {
      fprintf(stderr, "bind_socket - bind: %s\n", strerror(errno));
      err = 9;
    }
  else
    (void)fprintf(stdout, "server started at: %s:%d\n",
        inet_ntoa(info->addr.sin_addr),
        htons(info->addr.sin_port));
  return (err);
}

int			create_socket(int *sockfd)
{
  int			err;
  struct protoent	*s_protoent;

  err = 0;
  if (!(s_protoent = getprotobyname(PROTO)))
    (void)fprintf(stderr, "create_socket: unable to retrieve protocol name\n");
  else if ((*sockfd = socket(DOMAINE, TYPE, s_protoent->p_proto)) == -1)
    {
      (void)fprintf(stderr, "create_socket - socket: %s\n", strerror(errno));
      err = 8;
    }
  return (err);
}
