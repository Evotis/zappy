/*
** ai_buffer.c for ai_buffer in /home/fest/project/sysu/zappy/PSU_2013_zappy/server/src
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Mon Jul  7 11:59:48 2014 Adrien Chataignoux
** Last update Sun Jul 13 21:01:07 2014 Adrien Chataignoux
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ai_buffer.h"

t_reqtype		reqtype[11] =
  {
    {"avance", MOVE},
    {"droite", RIGHT},
    {"gauche", LEFT},
    {"voir", SEE},
    {"inventaire", INVENT},
    {"prend", TAKE},
    {"pose", DROP},
    {"expulse", EXPEL},
    {"incantation", INCANT},
    {"fork", FORKE},
    {"connect", CONNECTT}
  };

t_ittype		itemtype[7] =
  {
    {"nourriture", FOOD},
    {"linemate", LINEMATE},
    {"deraumere", DERAUMERE},
    {"sibur", SIBUR},
    {"mendiane", MENDIANE},
    {"phiras", PHIRAS},
    {"thystame", THYSTAME},
  };

t_aibuffer		*ai_create(size_t len)
{
  t_aibuffer		*cb;
  size_t		i;

  cb = NULL;
  if ((cb = malloc(sizeof(*cb))) != NULL)
    {
      cb->len = len;
      if ((cb->aidata = malloc(sizeof(*cb->aidata) * len)) != NULL)
	{
	  cb->read = 0;
	  cb->write = 0;
	  for (i = 0; i < len; ++i)
	    if ((cb->aidata[i] = malloc(sizeof(*cb->aidata[i]))) != NULL)
	      {
		(void)memset(cb->aidata[i]->broadcast, 0, LEN);
		cb->aidata[i]->request = DEFAULT;
	      }
	}
      else
	{
	  free(cb);
	  cb = NULL;
	}
    }
  return (cb);
}

void		ai_free(t_aibuffer *cb)
{
  size_t	i;

  if (cb == NULL)
    return ;
  for (i = 0; i < 2; ++i)
      free(cb->aidata[i]);
  free(cb->aidata);
  free(cb);
}

t_aidata		*ai_read(t_aibuffer *cb)
{
  t_aidata		*ret;

  ret = NULL;
  if (cb->aidata[cb->read]->request == DEFAULT)
    return (NULL);
  ret = cb->aidata[cb->read];
  cb->read = (cb->read + 1) % cb->len;
  return (ret);
}
