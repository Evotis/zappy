/*
** connection.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Fri Jul 11 14:17:18 2014 thomas nieto
** Last update Fri Jul 11 14:17:18 2014 thomas nieto
*/

#include "server.h"
#include "monitor_fds.h"

int		handle_connection(void)
{
  if (select_read_fd(g_server.info.sockfd, WAIT_TO_HANDLE_CO) > 0)
    return (accept_connection());
  return (0);
}
