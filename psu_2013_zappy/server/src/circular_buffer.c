/*
** circular_buffer.c for src in /home/n0t/dev/circular_buffer/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sat Jun 28 12:02:47 2014 thomas nieto
** Last update Sun Jul 13 20:11:09 2014 Adrien Chataignoux
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "circular_buffer.h"

/*
** With the size(len) given as parameter,
** cb_create will malloc a new circular buffer and all
** the structure inside it.
** It will also initialize them
*/
t_cbuffer		*cb_create(size_t len)
{
  t_cbuffer		*cb;
  size_t		i;

  if ((cb = malloc(sizeof(*cb))))
  {
    cb->len = len;
    if ((cb->data = malloc(sizeof(*cb->data) * len)))
    {
      cb->r = 0;
      cb->w = 0;
      for (i = 0; i < len; ++i)
        if ((cb->data[i] = malloc(sizeof(*cb->data[i]))))
        {
          (void)memset(cb->data[i], 0, sizeof(*cb->data[i]));
          cb->data[i]->sockfd = NOP;
        }
    }
    else
    {
      free(cb);
      cb = NULL;
    }
  }
  return (cb);
}

void		cb_free(t_cbuffer *cb)
{
  size_t	i;

  for (i = 0; i < cb->len; ++i)
    free(cb->data[i]);
  free(cb->data);
  free(cb);
}

/*
** this function copy the data parameter
** into the cb->w'nth data's slot of the
** circular buffer pointed by the cb's parameter
** then cb->w is incremented
*/
int		cb_push(t_cbuffer *cb, t_data *data)
{
  int		err;
  size_t	step;

  err = 1;
  step = (cb->w + 1) % cb->len;
  (void)memcpy(cb->data[cb->w], data, sizeof(*data));
  cb->w = step;
  err = 0;
  return (err);
}

/*
** this function will return a pointer to the
** cb->r'nth data's slot of the circular buffer pointed by
** the cb's parameter. then cb->r is incremented
*/
t_data		*cb_read(t_cbuffer *cb)
{
  t_data	*ret;

  ret = cb->data[cb->r];
  if (!ret->resp[RESP_PLAYER_POS][0] && !ret->resp[RESP_GRAPHIC_POS][0])
    cb->r = (cb->r + 1) % cb->len;
  else
    ret = NULL;
  return (ret);
}

t_data		*cb_get(t_cbuffer *cb, size_t pos)
{
  t_data	*ret;

  ret = cb->data[(cb->r + pos) % cb->len];
  if (!ret->resp[RESP_PLAYER_POS][0] && !ret->resp[RESP_GRAPHIC_POS][0])
    ret = NULL;
  return (ret);
}
