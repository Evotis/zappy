/*
** graphic_start.c for graphic_start in /home/fest/project/sysu/zappy/PSU_2013_zappy/server
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Tue Jul  8 14:49:04 2014 Adrien Chataignoux
** Last update Sun Jul 13 15:42:04 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <unistd.h>
#include "server.h"
#include "graphic_com.h"
#include "player_management.h"

static int		graphic_egg()
{
  int			err;
  int			i;

  i = 0;
  while (i < g_nb_connected)
    {
      if (g_player[i].info.sockfd == SOCK_OPEN)
	if ((err = new_player(g_graphic_client.sockfd, i)) != 0)
	  return (err);
      i++;
    }
  return (0);
}

int		graphic_start()
{
  int		err;
  int		i;

  i = 0;
  if ((err = map_size(g_graphic_client.sockfd)) != 0)
    return (err);
  if ((err = map_content(g_graphic_client.sockfd, 0)) != 0)
    return (err);
  if ((err = time_ask(g_graphic_client.sockfd, 0, NULL)) != 0)
    return (err);
  if ((err = team_names(g_graphic_client.sockfd)) != 0)
    return (err);
  while (i < g_nb_connected)
    {
      if ((err = new_player(g_graphic_client.sockfd, i)) != 0)
	return (err);
      i++;
    }
  if ((err = graphic_egg()) != 0)
    return (err);
  return (0);
}
