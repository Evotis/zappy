/*
** graphic_string2.c for graphic_string2 in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server/src
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Fri Jun 27 16:06:52 2014 Adrien Chataignoux
** Last update Sat Jul 12 23:58:10 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "server.h"
#include "graphic_com.h"

int		egg_birth(int fd, int playernum, char *instruction)
{
  char		*str;
  int		eggnb;

  (void)playernum;
  if ((str = malloc(10 * sizeof(char))) == NULL)
    return (4);
  eggnb = atoi(instruction + 4);
  sprintf(str, "eht %d\n", eggnb);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		egg_connect(int fd, int playernum, char *instruction)
{
  char		*str;
  int		eggnb;

  (void)playernum;
  if ((str = malloc(10 * sizeof(char))) == NULL)
    return (4);
  eggnb = atoi(instruction + 4);
  sprintf(str, "ebo %d\n", eggnb);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		egg_die(int fd, int playernum, char *instruction)
{
  char		*str;
  int		eggnb;

  (void)playernum;
  if ((str = malloc(10 * sizeof(char))) == NULL)
    return (4);
  eggnb = atoi(instruction + 4);
  sprintf(str, "edi %d\n", eggnb);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		time_ask(int fd, int playernum, char *instruction)
{
  char		*str;

  (void)playernum;
  (void)instruction;
  if ((str = malloc(10 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "sgt %d\n", g_opt.time_unit);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		team_win(int fd, char *instruction)
{
  if (write(fd, instruction, strlen(instruction)) == -1)
    return (11);
  return (0);
}
