/*
** monitors_fds.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Wed Jul 09 17:22:34 2014 thomas nieto
** Last update Sun Jul 13 12:19:51 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "server.h"
#include "main.h"
#include "monitor_fds.h"

/*
** check for fd to be ready in writing
*/
int		select_write_fd(int fd, struct timeval timeout)
{
  fd_set	wfds;
  int		err;

  printf("fd: %d\n", fd);
  FD_ZERO(&wfds);
  FD_SET(fd, &wfds);
  if ((err = select(fd + 1, NULL, &wfds, NULL, &timeout)) > -1)
    return (FD_ISSET(fd, &wfds) ? 1 : 0);
  if (err < 0)
    perror("select_write_fd - ");
  if (DEBUG)
    fprintf(stderr, "fd: %d - select_write_fds: %d\n", fd, err);
  return (err);
}

/*
** check for fd to be ready in writing
*/
int		select_read_fd(int fd, struct timeval timeout)
{
  fd_set	rfds;
  int		err;

  FD_ZERO(&rfds);
  if (fd > -1)
  {
    FD_SET(fd, &rfds);
    if ((err = select(fd + 1, &rfds, NULL, NULL, &timeout)) > 0)
      return (FD_ISSET(fd, &rfds));
    if (err < 0)
      perror("select_read_fd - ");
    if (DEBUG)
      fprintf(stderr, "fd: %d - select_read_fd: %d\n", fd, err);
  }
  return (-1);
}

/*
** monitor many fds for writing
*/
int		select_write_fds(int *fds, int *selected,
		int fd_size, struct timeval timeout)
{
  int		i;
  int		err;
  fd_set	wfds;
  int		fd_max;

  fd_max = 0;
  FD_ZERO(&wfds);
  for (i = 0; i < fd_size; ++i)
  {
    if (fds[i] > -1)
      FD_SET(fds[i], &wfds);
    if (fds[i] > fd_max)
      fd_max = fds[i];
  }
  if (DEBUG)
    fprintf(stderr, "write_fds - fd_max: %d\n", fd_max + 1);
  if ((err = select(fd_max + 1, NULL, &wfds, NULL, &timeout)) > -1)
  {
    for (i = 0; i < fd_size; ++i)
      selected[i] = FD_ISSET(fds[i], &wfds);
    return (err);
  }
  else
    perror("select_write_fds: ");
  return (-1);
}

/*
** monitor many fds for writing
*/
int		select_read_fds(int *fds, int *selected,
    int fd_size, struct timeval timeout)
{
  int		i;
  int		err;
  fd_set	rfds;
  int		fd_max;

  fd_max = 0;
  FD_ZERO(&rfds);
  for (i = 0; i < fd_size; ++i)
  {
    if (fds[i] > -1)
      FD_SET(fds[i], &rfds);
    if (fds[i] > fd_max)
      fd_max = fds[i];
  }
  if (DEBUG)
    fprintf(stderr, "read_fds - fd_max: %d\n", fd_max + 1);
  if ((err = select(fd_max + 1, &rfds, NULL, NULL, &timeout)) > -1)
  {
    for (i = 0; i < fd_size; ++i)
      selected[i] = FD_ISSET(fds[i], &rfds);
    return (err);
  }
  else
    perror("select_read_fds: ");
  return (-1);
}
