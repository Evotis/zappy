/*
** get_args.c for  in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy
**
** Made by thomas nieto
** Login   <n0t@epitech.net>
**
** Started on Sun Jun 22 00:03:59 2014 thomas nieto
** Last update Sun Jul 13 22:44:26 2014 Adrien Chataignoux
*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "main.h"

int		get_port(char *port,
			 __attribute__((unused))char *argv[])
{
  int		err;
  long int	tmp;

  err = 0;
  tmp = strtol(port, NULL, 10);
  if (errno == ERANGE)
    {
      fprintf(stderr, "Wrong port format. Exiting.\n");
      err = 1;
    }
  else
    g_opt.port = htons(tmp);
  return (err);
}

int		get_x(char *x,
		      __attribute__((unused))char *argv[])
{
  int		err;
  t_length	tmp;

  err = 0;
  tmp = strtol(x, NULL, 10);
  if (errno == ERANGE)
    {
      fprintf(stderr, "Wrong width format. Exiting.\n");
      err = 2;
    }
  else
    {
      if (tmp > 100)
	g_opt.x = 100;
      else
	g_opt.x = tmp;
    }
  return (err);
}

int		get_y(char *y,
		      __attribute__((unused))char *argv[])
{
  int		err;
  t_length	tmp;

  err = 0;
  tmp = strtol(y, NULL, 10);
  if (errno == ERANGE)
    {
      fprintf(stderr, "Wrong length format. Exiting.\n");
      err = 3;
    }
  else
    {
      if (tmp > 100)
	g_opt.y = 100;
      else
	g_opt.y = tmp;
    }
  return (err);
}

int		get_team_name(__attribute__((unused))char *team,
			      char *argv[])
{
  int		r;
  int		i;
  int		j;

  r = 0;
  j = -1;
  for (i = 0; argv[i]; ++i)
    if (!(strncmp(argv[i], "-n", 2)))
      j = i + 1;
  for (i = 0; j != -1 && argv[j] && argv[j][0] != '-'; ++i, ++j)
    if (!r && (g_opt.team =
          realloc(g_opt.team, (i + 2) * sizeof(*g_opt.team)))
        && (g_opt.team[i] = malloc(sizeof(**g_opt.team)))
        && (g_opt.team[i + 1] = malloc(sizeof(**g_opt.team)))
        && strlen(argv[j]) < 30)
    {
      g_opt.team[i]->name = argv[j];
      g_opt.team[i + 1]->name = NULL;
    }
    else
    {
      fprintf(stderr, "teams name must be less than 30 caracters\n");
      r = 4;
    }
  return (r);
}

int		get_time_unit(char *time_unit,
			      __attribute__((unused))char *argv[])
{
  int		err;
  t_time	tmp;

  err = 0;
  tmp = strtol(time_unit, NULL, 10);
  if (errno == ERANGE || !tmp)
    {
      fprintf(stderr, "Wrong time unit. Exiting.\n");
      err = 5;
    }
  else
    g_opt.time_unit = tmp;
  return (err);
}
