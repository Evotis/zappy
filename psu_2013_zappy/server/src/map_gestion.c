/*
** map_gestion.c for server in /home/abd-el_y/rendu/psu_2013_zappy/server
**
** Made by Abd-el rahman
** Login   <abd-el_y@epitech.net>
**
** Started on  Sat Jul 12 12:56:04 2014 Abd-el rahman
** Last update Sat Jul 12 21:42:35 2014 Abd-el rahman
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "server.h"
#include "circular_buffer.h"
#include "request.h"
#include "map_gestion.h"

static const t_delay_tab	g_delay_tab[7] =
  {
    {FOOD, DFOOD, 4},
    {LINEMATE, DLINEMATE, 6},
    {DERAUMERE, DDERAUMERE, 8},
    {SIBUR, DSIBUR, 10},
    {MENDIANE, DMENDIANE, 12},
    {PHIRAS, DPHIRAS, 14},
    {THYSTAME, DTHYSTAME, 16}
  };

void		death_player(int fd)
{
  t_data	tmp;

  memset(&tmp, 0, sizeof(tmp));
  cb_clear(fd);
  tmp.sockfd = fd;
  tmp.resp_time = NOW;
  sprintf(tmp.resp[1], "pdi\n");
  cb_push(g_resp_clients, &tmp);
  destroy_player(fd);
}

int		life_player_gestion(void)
{
  size_t       	i;

  i = 0;
  while (i < g_nb_connected)
    {
      if (get_current_time() >= g_player[i].need_eat)
	{
	  if (!g_player[i].bag.food)
	    death_player(g_player[i].info.sockfd);
	  else
	    {
	      g_player[i].bag.food -= 1;
	      g_player[i].need_eat = get_current_time();
	    }
	}
      ++i;
    }
  return (0);
}

int		item_appearance(int index)
{
  int		i;
  int		x;
  int		y;
  int		nb;

  srand(time(NULL));
  nb = (g_opt.x - 1) * (g_opt.y - 1) / g_delay_tab[index].freq;
  for (i = 0; i < nb; ++i)
    {
      x = rand() % (g_opt.x - 1);
      y = rand() % (g_opt.y - 1);
      add_item(&g_map[y][x].item, g_delay_tab[index].citem, 1);
    }
  return (0);
}

void		init_delay_item(void)
{
  int		i;
  t_data	tmp;

  memset(&tmp, 0, sizeof(tmp));
  for (i = 0; i < 7; ++i)
    {
      g_delay_item[i] = get_resp_time(g_delay_tab[i].delay);
      item_appearance(i);
    }
  tmp.resp_time = NOW;
  sprintf(tmp.resp[1], "bct\n");
  cb_push(g_resp_clients, &tmp);
}

int		item_appearance_gestion(void)
{
  int		i;
  t_data	tmp;

  memset(&tmp, 0, sizeof(tmp));
  for (i = 0; i < 7; ++i)
    {
      if (get_current_time() >= g_delay_item[i])
	{
	  g_delay_item[i] = get_resp_time(g_delay_tab[i].delay);
	  item_appearance(i);
	}
    }
  tmp.resp_time = NOW;
  sprintf(tmp.resp[1], "bct\n");
  cb_push(g_resp_clients, &tmp);
  return (0);
}
