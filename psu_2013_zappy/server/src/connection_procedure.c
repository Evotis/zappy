/*
** server.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Fri Jun 27 11:39:22 2014 thomas nieto
** Last update Sun Jul 13 23:13:42 2014 Adrien Chataignoux
*/

#include <sys/select.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "main.h"
#include "server.h"
#include "player_management.h"
#include "circular_buffer.h"
#include "graphic_com.h"
#include "server_time.h"
#include "monitor_fds.h"

/*
** This function will write response in circular buffer
** to init the connection
**
*/
static void		write_resp_init(t_info *info, char *type)
{
  t_data		tmp;
  int			pos;

  pos = find_player_from_fd(info->sockfd);
  printf("FDDDDDDDDDDDDDDDddd: %d\n", info->sockfd);
  (void)memset(&tmp, 0, sizeof(tmp));
  if (!strcmp(type, IS_GRAPHIC))
    graphic_start();
  else if (pos != -1)
  {
    printf("COUCOU\n");
    tmp.sockfd = info->sockfd;
    snprintf(tmp.resp[RESP_PLAYER_POS], LEN, "%d\n", g_player[pos].num);
    tmp.resp_time = NOW;
    cb_push(g_resp_clients, &tmp);
    snprintf(tmp.resp[RESP_PLAYER_POS], LEN, "%u %u\n", g_opt.x, g_opt.y);
    cb_push(g_resp_clients, &tmp);
    (void)launch_actions("pnw\n"
        , find_player_from_fd(g_player[pos].info.sockfd));
    cb_dump(g_resp_clients);
  }
  sleep(2);
}

/*
** for a graphic client it's simple:
** test whether another graphic client is in place
** and then initiate the structure for it
**
** in case of a player connection few more steps are required:
** test whether the team_name requested exist,
** then test if a slot is availabe for accept the connection.
** If conditions are satisfied, it send thoses datas to the
** global g_player and will attempt to answer.
*/
static int		init_client(t_info *info, char *team_name)
{
  int			i;
  int			err;
  int			free;

  err = 0;
  if (!team_name && g_graphic_client.sockfd == SOCK_OPEN)
    (void)memcpy(&g_graphic_client, info, sizeof(*info));
  else if (team_name)
  {
    for (i = 0; g_opt.team[i]->name; ++i)
      if (!(strncmp(g_opt.team[i]->name, team_name,
              strlen(g_opt.team[i]->name))))
      {
        if ((free = find_player_free()) != FULL)
        {
          err = init_player(info, free, i);
          return (err);
        }
        else
          return (15);
      }
    err = 14;
  }
  return (err);
}

/*
** this function will perfom a read able to retrieve the client type
** and will call the suitable init_client function
*/
static int		get_type(t_info *info)
{
  int			err;
  ssize_t		len;
  char			type[30];

  err = 0;
  len = 0;
  (void)memset(type, 0, 30);
  if ((select_read_fd(info->sockfd, WAIT_TO_IDENTITY) > 0)
      && (len = read(info->sockfd, &type, 29)) > 0)
  {
    type[len] = '\0';
    if (!strncmp(IS_GRAPHIC, type, strlen(IS_GRAPHIC)))
      err = init_client(info, NULL);
    else
      err = init_client(info, type);
    if (!err)
      write_resp_init(info, type);
  }
  else
  {
    fprintf(stderr, "connection timeout - \
        unable to identify client, socket will close\n");
    err = 13;
    }
  return (err);
}

/*
** ~~~~~~~~ Will wait ~~~~~~~~ for the client ready to read
** and send it to him a welcoming message, waiting for his
** answer supposed to be his type
*/
static int	identify(t_info *info)
{
  int		err;

  err = 0;
  if ((write(info->sockfd, WELCOME, WELCOME_LEN)) != -1)
  {
    if (DEBUG)
      fprintf(stderr, "sended %s to socket: %d\n",
          WELCOME_DEBUG, info->sockfd);
    err = get_type(info);
  }
  else
  {
    fprintf(stderr, "write failed - closing this connection\n");
    err = 11;
  }
  return (err);
}

/*
** first step in connection to server procedure:
** perfoming accept system call
** set it in readfds set
** and other verifications steps.
** Like the ones describe above.
*/
int		accept_connection(void)
{
  int		err;
  socklen_t	addr_len;
  t_info	info;

  err = 0;
  (void)memset(&info.addr, 0, sizeof(info.addr));
  addr_len = sizeof(info.addr);
  if ((info.sockfd = accept(g_server.info.sockfd,
          (struct sockaddr *)(&info.addr), &addr_len)) != -1)
  {
    if (DEBUG)
      print_infos(&info);
    if ((err = identify(&info)))
    {
      fprintf(stderr, "accept_connection: %d\n", err);
      death_player(info.sockfd);
    }
  }
  else
  {
    perror("accept failure: ");
    err = 12;
  }
  return (err);
}

