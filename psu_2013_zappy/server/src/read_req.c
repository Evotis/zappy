/*
** read_req.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sun Jul 13 02:29:26 2014 thomas nieto
** Last update Sun Jul 13 23:11:50 2014 Adrien Chataignoux
*/

#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "main.h"
#include "server.h"
#include "read_req.h"
#include "monitor_fds.h"

static void		choose_push(int pos, char *req)
{
  if (!strncmp(req, "broadcast", strlen("broadcast")))
    ai_push_broadcast(g_player[pos].ai_bc, req);
  else if (!strncmp(req, "pose", strlen("pose"))
      || !strncmp(req, "prend", strlen("prend")))
    ai_push_special(g_player[pos].ai_bc, req);
  else
    ai_push(g_player[pos].ai_bc, req);
}

int		read_req(void)
{
  int		err;
  int		i;
  char		req[LEN];

  err = 0;
  for (i = 0; i < g_nb_connected; ++i)
  {
    (void)memset(req, 0, LEN);
    printf("fd: %d\n", g_player[i].info.sockfd);
    if ((err = select_read_fd(g_player[i].info.sockfd, WAIT_TO_REQ)) > 0)
    {
      if ((err = read(g_player[i].info.sockfd, req, LEN - 1)) > -1)
      {
        req[err] = '\0';
        choose_push(i, req);
      }
      else
        perror("read request - read: ");
    }
  }
  return (err);
}
