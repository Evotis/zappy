/*
** player_management.c for player_management in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Fri Jun 27 12:17:33 2014 Adrien Chataignoux
** Last update Sun Jul 13 16:27:45 2014 Adrien Chataignoux
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "main.h"
#include "player_management.h"

int		find_player_free(void)
{
  int		i;

  for (i = 0; i < BACKLOG; ++i)
    if (g_player[i].info.sockfd == SOCK_OPEN)
      return (i);
  return (FULL);
}

int		find_player_from_fd(int fd)
{
  int		i;

  for (i = 0; i < g_nb_connected; ++i)
    if (g_player[i].info.sockfd == fd)
      return (i);
  return (-1);
}

int		*search_incant(int x, int y)
{
  int		*players;
  int		i;
  int		idy;

  idy = 0;
  if ((players = malloc(g_nb_connected * sizeof(int))) == NULL)
    return (NULL);
  (void)memset(players, -1, g_nb_connected);
  for (i = 0; i < g_nb_connected; ++i)
    if (g_player[i].pos.x == x && g_player[i].pos.y == y)
      players[idy] = i;
  return (players);
}

void		player_free(void)
{
  int	i;

  for (i = 0; i < BACKLOG; ++i)
    ai_free(g_player[i].ai_bc);
}
