/*
** tools.c for tools in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Fri Jun 27 11:44:57 2014 Adrien Chataignoux
** Last update Tue Jul  8 17:27:38 2014 Adrien Chataignoux
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char		*extract_broadcast(char *str)
{
  char		*extract;
  int		i;
  int		y;

  y = 0;
  i = 5;
  if ((extract = malloc((strlen(str) + 1) * sizeof(char))) == NULL)
    return NULL;
  while (str[i] && (str[i] >= '0' && str[i] <= '9'))
    i++;
  if (str[i] == ' ')
    i++;
  while (str[i])
    extract[y++] = str[i++];
  return (extract);
}

char		*build_string(char *str, int *i)
{
  int		y;
  char		*tmp;

  y = 0;
  if ((tmp = malloc(5 * sizeof(char))) == NULL)
    return (NULL);
  while (i[y] != -1)
    {
      sprintf(tmp, "%d ", i[y]);
      str = strcat(str, tmp);
      y++;
    }
  str[strlen(str) - 1] = '\n';
  return (str);
}

void		extract_pos(int *x, int *y, char *instruction)
{
  int		i;

  *x = atoi(instruction + 4);
  i = 4;
  while (instruction[i] >= '0' && instruction[i] <= '9')
    i++;
  while (instruction[i] == ' ')
    i++;
  *y = atoi(instruction + (i - 1));
}
