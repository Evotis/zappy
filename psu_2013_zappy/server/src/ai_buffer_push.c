/*
** ai_buffer_push.c for ai_buffer_push in /home/fest/project/sysu/zappy/PSU_2013_zappy/server
** 
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
** 
** Started on  Tue Jul  8 12:08:06 2014 Adrien Chataignoux
** Last update Sun Jul 13 15:49:29 2014 Adrien Chataignoux
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ai_buffer.h"

extern t_reqtype	reqtype[11];
extern t_ittype		itemtype[7];

int		ai_push_special(t_aibuffer *cb, char *instruction)
{
  int		i;

  if (cb == NULL)
    return (0);
  if (cb->aidata[cb->write]->request != DEFAULT)
    return (-1);
  if (cb->aidata[cb->write]->broadcast[0])
    (void)memset(cb->aidata[cb->write]->broadcast, 0, LEN);
  while (i < 11)
    {
      if (!strncmp(instruction, reqtype[i].name, 4))
	cb->aidata[cb->write]->request = reqtype[i].type;
      i++;
    }
  while (i < 7)
    {
      if (!strncmp(instruction + 5, itemtype[i].name, 5) ||
	  !strncmp(instruction + 4, itemtype[i].name, 5))
	cb->aidata[cb->write]->citem = itemtype[i].type;
      i++;
    }
  cb->write += 1;
  return (0);
}

int		ai_push_broadcast(t_aibuffer *cb, char *instruction)
{
  size_t	len;

  if (cb == NULL)
    return (0);
  if ((len = strlen(instruction) - 10) <= 0)
    return (-1);
  if (cb->aidata[cb->write]->request != DEFAULT)
    return (-1);
  if ((cb->aidata[cb->write]->broadcast[0]))
    (void)memset(cb->aidata[cb->write]->broadcast, 0, LEN);
  cb->aidata[cb->write]->request = BROAD;
  cb->aidata[cb->write]->citem = DEFAUT;
  (void)memset(cb->aidata[cb->write]->broadcast, 0, LEN);
  (void)memcpy(cb->aidata[cb->write]->broadcast
	       , instruction + 10, len > LEN ? LEN : len);
  cb->write += 1;
  return (0);
}

int		ai_push(t_aibuffer *cb, char *instruction)
{
  int		i;

  if (cb == NULL)
    return (0);
  if (cb->aidata[cb->write]->request != DEFAULT)
    return (-1);
  if (cb->aidata[cb->write]->broadcast[0])
    (void)memset(cb->aidata[cb->write]->broadcast, 0, LEN);
  while (i < 11)
    {
      if (!strncmp(instruction, reqtype[i].name, 4))
	cb->aidata[cb->write]->request = reqtype[i].type;
      i++;
    }
  if (cb->aidata[cb->write]->request == DEFAULT)
    return (0);
  printf("%d\n", cb->aidata[cb->write]->request);
  cb->aidata[cb->write]->citem = DEFAUT;
  cb->write += 1 % 10;
  return (0);
}
