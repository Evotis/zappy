/*
** destroy_procedure.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Jul 07 16:19:33 2014 thomas nieto
** Last update Sun Jul 13 12:17:32 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "server.h"
#include "player_management.h"

/*
** function will either close a peer socket
** either than clear his fd from the fdset
** containing in t_server structure
*/
int		close_connection(int fd)
{
  int		err;

  err = 0;
  if (DEBUG)
    fprintf(stderr, "Closing %d socket\n", fd);
  if ((close(fd)) == -1)
  {
    perror("close connection: ");
    err = 255;
  }
  return (err);
}

/*
** This fancy function, allows not just to destroy a client
** but, it will, also, reconfigure the g_player array.
**
** It will:
** find a player's position in g_player's array thanks to his fd,
** and memcpy the g_nb_connected - 1 player in found position
** and memset 0 his structure in array
** finally, g_nb_connected will be decremented
*/
void		destroy_player(int sockfd)
{
  ssize_t	pos;
  int	i;

  if ((pos = find_player_from_fd(sockfd)) != -1)
  {
    for (i = 0; g_opt.team[i]->name; ++i)
      if (!strcmp(g_opt.team[i]->name, g_player[pos].team_name))
      {
        g_opt.team[i]->nb += 1;
        break;
      }
    (void)memcpy(&g_player[pos],
        &g_player[g_nb_connected - 1], sizeof(*g_player));
    (void)memset(&g_player[g_nb_connected - 1], 0, sizeof(*g_player));
    init_one_player(g_nb_connected - 1);
    if (g_opt.team[i]->nb)
      g_player[pos].info.sockfd = SOCK_OPEN;
    g_nb_connected -= 1;
  }
  close_connection(sockfd);
}

