/*
** routine.c for src in /home/n0t/dev/system-unix/zappy/PSU_2013_zappy/server/src
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Fri Jun 27 22:42:27 2014 thomas nieto
** Last update Sun Jul 13 21:17:11 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <signal.h>

#include "main.h"
#include "request.h"
#include "server.h"
#include "write_resp.h"
#include "read_req.h"

static int	g_stop = 0;

void		handler(int sig)
{
  (void)sig;
  g_stop = 1;
  if (DEBUG)
    printf("signal caught\n");
}

static int	play_again(void)
{
  int		i;

  for (i = 0; i < g_nb_connected; ++i)
    if (g_player[i].level == 8)
    {
      fprintf(stdout, "One player is level 8, end of game. Thank you \
to have played with us!\n");
      return (0);
    }
  return (1);
}

int		routine(void)
{
  int		err;
  int		ret;

  err = 0;
  (void)err;
  while (!g_stop && (ret = play_again()))
  {
    puts("one turn");
    check_incant();
    err = handle_connection();
    (void)read_req();
    (void)life_player_gestion();
    (void)item_appearance_gestion();
    (void)do_req();
    (void)write_resp();
    signal(SIGINT, handler);
    print_players();
  }
  return (ret);
}
