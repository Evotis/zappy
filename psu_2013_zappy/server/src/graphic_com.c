/*
** graphic_com.c for  in /home/fest/project/sysu/zappy/projet/client_dir/PSU_2013_zappy/server/src
**
** Made by Adrien Chataignoux
** Login   <fest@epitech.net>
**
** Started on  Wed Jun 25 16:48:10 2014 Adrien Chataignoux
** Last update Sun Jul 13 15:18:26 2014 Adrien Chataignoux
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "server.h"
#include "graphic_com.h"
#include "player_management.h"

static const t_com	g_tab_com[22] =
{
  {"pin\n", player_inventory},
  {"pic\n", player_incant},
  {"pfk\n", player_egg},
  {"pnw\n", new_player},
  {"ppo\n", player_pos},
  {"plv\n", player_level},
  {"pex\n", expulsion},
  {"pdi\n", player_starvation},
  {"pbc\n", player_broadcast},
  {"pie\n", player_incant_end},
  {"pdr\n", player_drop},
  {"pgt\n", player_take},
  {"enw\n", egg_laid},
  {"eht\n", egg_birth},
  {"ebo\n", egg_connect},
  {"edi\n", egg_die},
  {"sgt\n", time_ask},
  {"bct\n", map_case},
  {"msz\n", map_size},
  {"tna\n", team_names},
};

int		launch_actions(char *instruction, int playernum)
{
  int		i;

  i = 0;
  if (playernum == -1 || g_graphic_client.sockfd == SOCK_OPEN)
    return (-1);
  while (i < 9)
    {
      if (!strncmp(g_tab_com[i].key, instruction, 3))
	return (g_tab_com[i].ptr(g_graphic_client.sockfd, playernum));
      ++i;
    }
  while (i < 18)
    {
      if (!strncmp(g_tab_com[i].key, instruction, 3))
	return (g_tab_com[i].ptr(g_graphic_client.sockfd, playernum, instruction));
      ++i;
    }
  while (i < 20)
    {
      if (!strncmp(g_tab_com[i].key, instruction, 3))
	return (g_tab_com[i].ptr(g_graphic_client.sockfd));
      ++i;
    }
  return (unknown_cmd(g_graphic_client.sockfd));
}

int		player_egg(int fd, int playernum)
{
  char		*str;

  if ((str = malloc(10 * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "pfk %d\n", playernum);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		map_content(int fd, int playernum)
{
  char		*str;
  int		x;
  int		y;
  t_item	it;

  (void) playernum;
  if ((str = malloc(100 * sizeof(char))) == NULL)
    return (4);
  for (x = 0; x < g_opt.x; ++x)
    for (y = 0; y < g_opt.y; ++y)
      {
	it = g_map[y][x].item;
	sprintf(str, "bct %d %d %d %d %d %d %d %d %d\n", x, y, it.food,
		it.linemate, it.deraumere, it.sibur,
		it.mendiane, it.phiras, it.thystame);
	if (write(fd, str, strlen(str)) == -1)
	  return (11);
      }
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_inventory(int fd, int playernum)
{
  char		*str;
  t_item	bag;

  if ((str = malloc(80 * sizeof(char))) == NULL)
    return (4);
  bag = g_player[playernum].bag;
  sprintf(str, "pin %d %d %d %d %d %d %d %d %d %d\n", playernum
	  , g_player[playernum].pos.x, g_player[playernum].pos.x, bag.food
	  , bag.linemate, bag.deraumere, bag.sibur, bag.mendiane, bag.phiras
	  , bag.thystame);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}

int		player_incant(int fd, int playernum)
{
  char		*str;
  int		*players;
  int		y;

  y = 0;
  players = search_incant(g_player[playernum].pos.x
			  , g_player[playernum].pos.y);
  while (players[y] != -1)
    y++;
  if ((str = malloc(((5 * y) + 20) * sizeof(char))) == NULL)
    return (4);
  sprintf(str, "pic %d %d %d ", g_player[playernum].pos.x
	  , g_player[playernum].pos.y,  g_player[playernum].level);
  str = build_string(str, players);
  if (write(fd, str, strlen(str)) == -1)
    return (11);
  free(str);
  return (0);
}
