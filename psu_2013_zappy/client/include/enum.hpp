/*
** enum.hpp for we in /home/cristi_a/save/stand
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Sat Jun  21 17:34:18 2014 toma cristini
** Last update Sat Jun  21 17:34:18 2014 toma cristini
*/

#ifndef ENUM_HPP
#define ENUM_HPP

enum eKip
{
	STARK,
	LANNISTER,
	BARATHEON,
	TARGARYEN,
	ALONE
};

enum eTat
{
	OCCUPE,
	DISPONIBLE
};

enum eOrientation
{
	UP,
	RIGHT,
	DOWN,
	LEFT
};

enum eAction
{
	CHECK_STATE,
	TEAMWORK,
	VISION,
	BUILD_WAY,
	EXECUTE_WAY,
	ROTATE_LEFT,
	ROTATE_RIGHT,
	TURN_BACK,
	TAKE_OBJ,
	MOVE,
	MOVUP,
	MOVDOWN,
	MOVRIGHT,
	MOVLEFT,
	MOVING_TO_YOU,
	IM_THERE,
	ASK_FRIENDS,
	INCANT,
	CHECK_TEAM,
	MOBILIZ_ARMY
	// TAKE,
	// ROTATE,
	// MOVE,
	// BROADCAST,
	// DROP,
	// SEE,
	// EXPEL,
	// INCANT,
	// FORK,
	// DEATH
};

#endif /* !ENUM_HPP */