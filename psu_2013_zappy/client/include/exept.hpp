#ifndef EXECPT_HH_
#define EXECPT_HH_

#include <exception>

class ConnectError : public std::exception
{
public:
	ConnectError(std::string const& phrase="") throw()
	:m_phrase(phrase)
	{}

	virtual const char* what() const throw()
	{
		return m_phrase.c_str();
	}

	virtual ~ConnectError() throw()
	{}

private:
	std::string m_phrase;
};

#endif