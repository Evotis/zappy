/*
** trantorien.hpp for  in /home/cristi_a/save/stand
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Sat Jun  21 17:02:06 2014 toma cristini
** Last update Sat Jun   21 17:02:06 2014 toma cristini
*/

#ifndef TRANTORIEN_HPP
#define TRANTORIEN_HPP

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <stack>
#include <boost/regex.hpp>
#include <math.h>
#include "enum.hpp"
#include "network/Network.hpp"
#include "exept.hpp"

#define RED "\033[31m"
#define GREEN "\033[32m"
#define	YELLOW "\033[33m"
#define	BLUE "\033[34m"
#define	MAGENTA "\033[35m"
#define CYA "\033[36m"
#define END "\033[0m"

typedef std::string 																		eObj;
typedef std::pair<int, int> 														t_position;
typedef std::map<eObj, int>															t_inventaire;
typedef std::vector<std::pair<int, t_inventaire> >			t_element;
typedef std::pair<int, std::string>											t_msg;
typedef	std::list<t_msg >																t_msg_list;
typedef std::vector<std::pair<t_position, t_inventaire> > t_chemin;

class Trantorien
{
	typedef std::map<eAction, eAction	(Trantorien::*)()> 											t_instructions;
	typedef std::map< std::string, void (Trantorien::*)(std::string const &)> t_server_treat;
	typedef std::map< std::string, eAction (Trantorien::*)(t_msg &)>					t_broadcast_treat;

	int 								lifetime;
	int 								level_;
	int 								team_id;
	int 								connect_nb;
	bool								alive_;
	bool								pause_incant;
	bool								vision_incoming;
	bool								can_talk;
	t_inventaire				objective;
	eTat								state;
	t_msg_list					in_team_msg;
	eAction							redirection;
	t_chemin						chemin;
	t_element						c_vision;
	t_position					position;
	eOrientation				direction;
	t_inventaire				inventaire;
	t_instructions			instr;
	t_server_treat			msg_server;
	t_broadcast_treat		msg_team;

	std::map<int, t_inventaire> 				required;
	std::map<eOrientation, eAction>			call_m;
	std::map<eOrientation, t_position>	next_move;

	//std::stack<Request *>			requestList;

public:
	Network 				net_;
	Trantorien(t_param &);
	Trantorien(Trantorien const &);
	Trantorien& operator=(const Trantorien&);
	~Trantorien();

	bool					start();
	bool 					blind();
	bool					occupe();
	bool					disponible();

	void 					takeFood();
	void 					getVision();
	bool 					followTheWay();
	bool					analyseVision();
	bool					analyseCase(t_inventaire &, t_inventaire &);
	bool					readyIncant(t_inventaire &);
	t_position		getPosition(int i);

	eAction				buildWay();
	eAction				checkState();
	eAction				dirDown();
	eAction				dirLeft();
	eAction				dirRight();
	eAction				dirUp();
	eAction				executeWay();
	eAction				move();
	eAction				rotateLeft();
	eAction				rotateRight();
	// eAction				takeObj();
	eAction				teamwork();
	eAction				vision();
	eAction				incant();
	eAction				askFriends();
	eAction				joinFriend(int, std::string const &);
	eAction				movingToYou();
	eAction				imThere();
	eAction				turnBack();
	eAction				mobilizArmy();
	eAction				lvl_protocol(t_inventaire &);

	void					treatMessage(std::string const &);
	void					treatServer();

	void		      treatConfirmation(std::string const &);
	void		      treatFailure(std::string const &);
	void		      treatMort(std::string const &);
	void		      treatBroadcast(std::string const &);
	void		      treatExpulse(std::string const &);
	void		      treatVision(std::string const &);
	void		      treatInventory(std::string const &);
	void		      treatConnections(std::string const &);
	void		      treatIncant(std::string const &);
	void		      treatLevel(std::string const &);

	void 					take(eObj, int i);
	void					drop(eObj, int i);
	void 					getNeeded();
	// eObj 					&nextObjectiv(t_inventaire & needed);
	void					dropItem();
	void 					positionReached(t_inventaire &caze);
	void					level();
	void					reset(std::vector<int> &);
	void					createRequired(t_inventaire & req, std::vector<int> & val);

	void					printDirection();
	void 					init();
	void 					initInstr();
	void 					initMsgServ();
	void 					initMsgTeam();
	void 					initInventaire(t_inventaire &);
	void 					initMove();

	t_position 		getBroadcastDirection(int i);
	eAction				checkTeam();
	eAction				teamNeedIncant(t_msg &);
	eAction				teamReadyIncant(t_msg &);
	eAction				teamNewPlayer(t_msg &);
	eAction				teamNb(t_msg &);
	eAction				teamCoNb(t_msg &);
};

#endif /* !TRANTORIEN_HPP */