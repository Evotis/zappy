/*
** Network.hpp for network in /home/mohame_e/project/SysUnix/PSU_2013_zappy/client/include/network
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Fri Jun 20 19:25:43 2014 Ahmed Mohamed Ali
** Last update Fri Jun 20 19:25:43 2014 Ahmed Mohamed Ali
*/

#ifndef NETWORK_H_
# define NETWORK_H_

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include "exept.hpp"

typedef struct  s_param
{
  std::string team;
  int         port;
  std::string machine;
}             t_param;

typedef std::list<std::string>   t_comList;

class Network
{
  int         fd_;
  int         worldX_;
  int         worldY_;
  t_comList   in_;
  t_comList   out_;
  t_comList   pending_;

  public:
    Network();
    ~Network();

  int         clientsLeft_;
  std::string team_;

  t_comList     &getOutCommunication();
  t_comList     &getInCommunication();

  bool          checkServ();
  void          checkPending();
  bool          garantAccess();
  bool          initConnection(t_param &);
  void          writeServ(std::string const &);
  void          sendMessage(std::string const &);
  std::string   readServ();
};


#endif  /* !NETWORK_H_ */
