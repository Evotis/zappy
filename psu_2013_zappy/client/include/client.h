/*
** client.h for client in /home/fest/project/sysu/zappy/projet/client_dir
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Sun May 25 15:59:32 2014 Adrien Chataignoux
** Last update Sun May 25 16:44:43 2014 Adrien Chataignoux
*/

#ifndef CLIENT_H_
# define CLIENT_H_

#include <string>
#include <iostream>

typedef struct	s_param
{
	std::string team;
	int			port;
	std::string	machine;
}				t_param;

typedef struct	s_client
{
	std::string team;
	int 		fd;
	int 		x;
	int 		y;
}				t_client;

#endif /* !CLIENT_H_ */
