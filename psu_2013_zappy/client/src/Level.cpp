/*
** Level.cpp for we in /home/cristi_a/rendu/PSU_2013_zappy/client/src
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Wed Jul  02 14:57:46 2014 toma cristini
** Last update Wed Jul  02 14:57:46 2014 toma cristini
*/

#include "Trantorien.hpp"

void		Trantorien::getNeeded()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << "Next Level : " << level_ + 1 << std::endl;
	objective["linemate"] = required[level_]["linemate"] - inventaire["linemate"];
	objective["deraumere"] = required[level_]["deraumere"] - inventaire["deraumere"];
	objective["sibur"] = required[level_]["sibur"] - inventaire["sibur"];
	objective["mendiane"] = required[level_]["mendiane"] - inventaire["mendiane"];
	objective["phiras"] = required[level_]["phiras"] - inventaire["phiras"];
	objective["thystame"] = required[level_]["thystame"] - inventaire["thystame"];
	objective["nourriture"] = 1;
	// needed["joueur"] = required[i]["joueur"] - 1;
}

// eObj 		&Trantorien::nextObjectiv(t_inventaire & needed)
// {
// 	t_inventaire::iterator it;

// 	if (inventaire["nourriture"] < 6)
// 		return ("nourriture");

// 	//// std::cout << GREEN << "nextObjectiv" << END << " :";
// 	for (it = needed.begin(); it != needed.end(); ++it)
// 	{
// 		if (it->second > 0)
// 			{
// 				//// std::cout << it->first << "[" << RED << it->second << END << "]" << std::endl;
// 					return (it->first);
// 			}
// 		// std::cout<< it->first << "[" << CYA << it->second << END << "] ";
// 	}
// 	// std::cout << "empty_case [" << RED << "1" << END << "]" << std::endl;
// 	return ("empty_case");
// }

void Trantorien::level()
{
	std::vector<int> val;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	reset(val);
	val[0] = 1;
	val[1] = 1;

	createRequired(required[1], val);

	val[0] = 2;
	val[2] = 1;
	val[3] = 1;

	createRequired(required[2], val);

	val[1] = 2;
	val[2] = 0;
	val[5] = 2;

	createRequired(required[3], val);

	val[0] = 4;
	val[1] = 1;
	val[2] = 1;
	val[3] = 2;
	val[5] = 1;

	createRequired(required[4], val);

	val[2] = 2;
	val[3] = 1;
	val[4] = 3;
	val[5] = 0;

	createRequired(required[5], val);

	val[0] = 6;
	val[3] = 3;
	val[4] = 0;
	val[5] = 1;

	createRequired(required[6], val);

	val[1] = 2;
	val[3] = 2;
	val[4] = 2;
	val[5] = 2;
	val[6] = 1;

	createRequired(required[7], val);
}

void	Trantorien::reset(std::vector<int> & val)
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	for (int i = 0; i < 7; ++i)
		val.push_back(0);
}

void	Trantorien::createRequired(t_inventaire & req, std::vector<int> & val)
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << "createRequired" << std::endl;
	req["joueur"] = val[0];
	// std::cout << "\tjoueur : " << val[0];
	req["linemate"] = val[1];
	// std::cout << "\tlinemate : " << val[1];
	req["deraumere"] = val[2];
	// std::cout << "\tderaumere : " << val[2];
	req["sibur"] = val[3];
	// std::cout << "\tsibur : " << val[3];
	req["mendiane"] = val[4];
	// std::cout << "\tmendiane : " << val[4];
	req["phiras"] = val[5];
	// std::cout << "\tphiras : " << val[5];
	req["thystame"] = val[6];
	// std::cout << "\tthystame : " << val[6] << std::endl;
}