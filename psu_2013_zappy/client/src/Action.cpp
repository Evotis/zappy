#include "Trantorien.hpp"
#include "exept.hpp"

eAction			Trantorien::checkState()
{
	if (pause_incant)
	{
		std::cout << MAGENTA << __FUNCTION__ << "(pause_incant)" << END << std::endl;
		return (CHECK_STATE);
	}
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// // std::cout << "Nourriture [" << YELLOW << inventaire["nourriture"] << END << "] ";
	// // std::cout << "linemate [" << YELLOW << inventaire["linemate"] << END << "]" << std::endl;
	getNeeded();
	return (VISION);
}

void 			Trantorien::drop(eObj o, int i)
{
	std::cout << MAGENTA << __FUNCTION__ << GREEN << std::endl;
	while (i > 0)
	{
		std::cout << "pose " << o << "\t";
		net_.sendMessage("pose " + o);
		i--;
	}
	std::cout << END << std::endl;
}

void 			Trantorien::take(eObj o, int i)
{
	std::cout << MAGENTA << __FUNCTION__ << GREEN << std::endl;
	while (i > 0)
	{
		std::cout << "prend " << o << "\t";
		net_.sendMessage("prend " + o);
		i--;
	}
	std::cout << END << std::endl;
}

eAction		Trantorien::incant()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	std::cout << GREEN << "\tincantation" << END << std::endl;
	pause_incant = true;
	vision_incoming = false;
	net_.sendMessage("incantation");
	return (CHECK_STATE);
}


// A NE PAS METTRE DANS LE POINTEUR SUR FONCTION SINN NBTM
eAction		Trantorien::lvl_protocol(t_inventaire &caze)
{
	t_inventaire o = required[level_];
	int i;

	for (t_inventaire::iterator it = o.begin(); it != o.end(); ++it)
	{
		if (it->first == "joueur")
			continue;
		if ((i = it->second - caze[it->first]) > 0)
			drop(it->first, i);
		else if ((i = it->second - caze[it->first]) < 0)
			take(it->first, i);
	}
	if (caze["joueur"] > o["joueur"])
		net_.sendMessage("expulse");
	return (ASK_FRIENDS);
}

bool			Trantorien::readyIncant(t_inventaire &caze)
{
	t_inventaire o = required[level_];

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	for (t_inventaire::iterator it = o.begin(); it != o.end(); ++it)
	{
		if (it->first == "joueur")
			continue;
		if (it->second > inventaire[it->first] + caze[it->first])
			return false;
	}
	return true;
}

eAction			Trantorien::vision()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	if (!vision_incoming)
		{
		std::cout << GREEN << "\tvoir" << END << std::endl;
		net_.sendMessage("voir");
		vision_incoming = true;
		position = std::make_pair(0, 0);
		direction = UP;
		}
	if (analyseVision())
		return (BUILD_WAY);
	redirection = VISION;
	return (CHECK_TEAM);
	//{
		//nb_rotate = 0;
	//}
	//if (objective == PLAYER)
	// 	return (ASK_FRIENDS);
	//nb_rotate++;
	//if (nb_rotate < 3){
	//	redirection = VISION;
	//	return (ROTATE_LEFT);
	//}
	//return (MOVE);
}

bool 	Trantorien::analyseVision()
{
	t_inventaire tmp = objective;

	if (c_vision.size() == 0)
	{
		// std::cout << MAGENTA << "Waiting for vision" << END << std::endl;
		return false;
	}
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	std::cout << GREEN << "\tinventaire" << END << std::endl;
	net_.sendMessage("inventaire");
	vision_incoming = false;
	// std::cout << GREEN << "analyseVision " << END;
	for (t_element::iterator it = c_vision.begin(); it != c_vision.end(); ++it)
	{
		if (analyseCase(it->second, tmp))
			return (true);
	}
	c_vision.clear();
	move();
	// std::cout << std::endl;
	return false;
}

bool	Trantorien::analyseCase(t_inventaire &caze, t_inventaire &tmp)
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	for (t_inventaire::iterator it = caze.begin(); it != caze.end(); ++it)
	{
		if (tmp[it->first] >= 0 && it->second >= 0)
		{
			tmp[it->first] -= 1;
			std::cout << "\t\t" << it->first << " needed" << std::endl;
			// std::cout << it->first << std::endl;
			return (true);
		}
	}
	return (false);
}

eAction			Trantorien::buildWay()
{
	t_inventaire		tmp = objective;
	
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	chemin.clear();
	for (t_element::iterator it = c_vision.begin(); it != c_vision.end(); ++it)
	{
		if (analyseCase(it->second, tmp))
				chemin.push_back(std::make_pair(getPosition(it->first), it->second));
	}
	c_vision.clear();
	return (EXECUTE_WAY);
}

eAction			Trantorien::executeWay()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	for (t_chemin::iterator it = chemin.begin(); it != chemin.end(); ++it)
	{
		if (it->first == position)
		{
			if (readyIncant(it->second))
				return (lvl_protocol(it->second));
			positionReached(it->second);
			chemin.erase(it);
			if (chemin.empty())
				return (CHECK_STATE);
			it = chemin.begin();
		}
	}
	if (chemin.empty())
		return (CHECK_STATE);
	redirection = EXECUTE_WAY;
	return ((this->*instr[call_m[direction]])());
}

void 			Trantorien::positionReached(t_inventaire & caze)
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	for (t_inventaire::iterator it = caze.begin(); it != caze.end(); ++it)
	{
		if (objective[it->first] > 0 && it->second > 0)
		{
			std::cout << GREEN << "\tprend " << it->first << END << std::endl;
			net_.sendMessage("prend " + it->first);
		}
	}
}