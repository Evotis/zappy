/*
** broadcast.cpp for src in /home/mohame_e/project/SysUnix/PSU_2013_zappy/client/src
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Sat Jul 12 17:51:19 2014 Ahmed Mohamed Ali
** Last update Sat Jul 12 17:51:19 2014 Ahmed Mohamed Ali
*/
#include "Trantorien.hpp"

eAction       Trantorien::checkTeam() {
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  eAction     ret;

  while (!in_team_msg.empty()) {
    // std::cout << "current msg :" << in_team_msg.front().second << std::endl;
    for (t_broadcast_treat::iterator m = msg_team.begin(); m != msg_team.end(); ++m) {
        // std::cout << RED << "REGEX MATCH " << in_team_msg.front().second << " with " << m->first << END << std::endl;
      if (boost::regex_match(in_team_msg.front().second, boost::regex(m->first))) {
        if ((ret = (this->*(m->second))(in_team_msg.front())) != CHECK_STATE) {
          in_team_msg.pop_front();
          return ret;
        }
      }
    }
    in_team_msg.pop_front();
  }
  return CHECK_STATE;
}

eAction       Trantorien::teamNeedIncant(t_msg &m) {
  std::stringstream   ss;
  std::string         lvl_val;
  int                 rl;

  for (t_inventaire::iterator it = inventaire.begin(); it != inventaire.end(); ++it) {
    if (required[level_][it->first] > it->second && it->first != "joueur")
      return CHECK_STATE;
  }
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  // std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  lvl_val = m.second.substr(m.second.size() - 2);
  ss << lvl_val;
  ss >> rl;
  if (rl != level_ + 1)
    return CHECK_STATE;
  std::cout << "I AM FUCKING READY TO INCANT AT LVL " << level_ << YELLOW << m.second << END << std::endl;
  net_.sendMessage("broadcast " + net_.team_ + " ri " + std::to_string(team_id) + " " + std::to_string(level_ + 1));
  joinFriend(m.first, m.second);
  return CHECK_STATE;
}

eAction       Trantorien::teamNewPlayer(t_msg &m) {
  // (void)m;

  std::cout << YELLOW << m.second << END << std::endl;
  std::cout << "my team id" << team_id << std::endl;
  if (team_id == 0) {
    net_.sendMessage("broadcast " + net_.team_ + " nb " + std::to_string(connect_nb));
    connect_nb += 1;
    net_.sendMessage("broadcast " + net_.team_ + " co_nb " + std::to_string(connect_nb));
  }
  return CHECK_STATE;
}

eAction       Trantorien::teamReadyIncant(t_msg &m) {
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  // std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  // // std::cout << "RECEIVED READY from : " << std::atoi(m.second.substr(net_.team_.size() + 4)) << "at " <<m.second << std::endl;
  (void)m;
  std::cout << "RECEIVED READ" << std::endl;
  std::cout << m.second << std::endl;
  net_.sendMessage("broadcast " + net_.team_ + " ni " + std::to_string(level_ + 1));
  return CHECK_STATE;
}

eAction       Trantorien::teamNb(t_msg &m) {
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  std::string       str = m.second.substr(net_.team_.size() + 4);

  std::cout << m.second << std::endl;
  if (team_id == 0) {
    std::stringstream ss;
    ss << str;
    ss >> team_id;
  }
  return CHECK_STATE;
}

eAction       Trantorien::teamCoNb(t_msg &m) {
  std::string str = m.second.substr(net_.team_.size() + 7);
  std::stringstream ss;

  std::cout << m.second << std::endl;
  ss << str;
  ss >> connect_nb;
  // std::cout << "connected : " <<  connect_nb << std::endl;
  return CHECK_STATE;
}