/*
** Network.cpp for network in /home/mohame_e/project/SysUnix/PSU_2013_zappy/client/src/network
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Sun Jun 22 12:31:00 2014 Ahmed Mohamed Ali
** Last update Sun Jun 22 12:31:00 2014 Ahmed Mohamed Ali
*/

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "network/Network.hpp"

Network::Network() {
}

Network::~Network() { }

bool     Network::initConnection(t_param &p) {
  struct sockaddr_in    s_in;
  struct protoent       *pe;

  if (!(pe = getprotobyname("TCP"))) {
    perror("getprotobyname");
    return false;
  }
  s_in.sin_family = AF_INET;
  s_in.sin_port = htons(p.port);
  s_in.sin_addr.s_addr = inet_addr(p.machine.c_str());
  if ((fd_ = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1) {
    perror("Socket");
    return false;
  }
  if (connect(fd_, (struct sockaddr *)&s_in, sizeof(s_in)) == -1) {
    close(fd_);
    perror("Connect");
    return false;
  }
  // std::cout << "Connected to :" << p.machine << ":" << p.port << std::endl;
  return true;
}

bool      Network::garantAccess() {
  std::string       str;
  std::stringstream ss;

  str = readServ();
  if (str == "BIENVENUE")
    sendMessage(team_);
  else
    throw ConnectError("The server cannot handle any more connection for this team");
  str = readServ();
  ss << str;
  ss >> clientsLeft_;
  // str = readServ();
  // ss << str;
  // ss >> worldX_;
  // ss >> worldY_;
  in_.clear();
  out_.clear();
  return true;
}

#include <strings.h>

std::string   Network::readServ() {
  char        line[4096];
  int         r;
  std::string str;
  std::size_t x;

  bzero(line, 4096);
  if ((r = read(fd_, line, 4096) == -1) && errno != EAGAIN)
    throw ConnectError("The connection with the server hung up unexpectedly");
  else if (r != -1) {
    str = line;
    while ((x = str.find('\n')) != str.npos) {
      in_.push_back(str.substr(0, x));
      str = str.substr(x + 1);
    }
  }
  return in_.front();
}
void      Network::writeServ(std::string const &s) {
  std::string str = s + "\n";

  // std::cout << "sending " << s << std::endl;
  if ((write(fd_, str.c_str(), str.size())) == -1)
    throw ConnectError("Write failure");
  out_.push_back(s);
}

bool        Network::checkServ() {
  fd_set        fd_read;
  struct timeval    tv;

  FD_ZERO(&fd_read);
  tv.tv_usec = 0;
  tv.tv_sec = 0;
  FD_SET(fd_, &fd_read);
  if (select(fd_ + 10, &fd_read, NULL, NULL, &tv) == -1) {
    return false;
  }
  if (FD_ISSET(fd_, &fd_read))
  {
    try {
      readServ();
      checkPending();
    }
    catch (std::exception const &e) {
      // std::cout << e.what() << std::endl;
      return false;
    }
  }
  return true;
}

void       Network::checkPending() {
  while (!pending_.empty() && out_.size() < 10) {
    writeServ(pending_.front());
    pending_.pop_front();
  }
}

void       Network::sendMessage(std::string const &s) {
  // std::cout << "\t\t" << s << std::endl;
  while (!pending_.empty() && out_.size() < 10) {
    writeServ(pending_.front());
    pending_.pop_front();
  }
  if (out_.size() < 10)
    writeServ(s);
  else
    pending_.push_back(s);
}

t_comList &Network::getOutCommunication() {
  return (out_);
}

t_comList &Network::getInCommunication() {
  return (in_);
}
