#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "client.h"
#include "exept.hpp"

std::string 			read_serv(int fd)
{
	FILE 				*fp;
	char 				*line = NULL;
	size_t 				len = 0;
	ssize_t 			read;
	std::string			str;

	fp = fdopen(fd, "r");
	if (fp == NULL)
		throw ConnectError("The connection with the server hung up unexpectedly");
	if ((read = getline(&line, &len, fp)) == -1)
		throw ConnectError("The connection with the server hung up unexpectedly");
	str = line;
	// std::cout << line << std::endl;
	free(line);
	return str;
}

int						can_connect(t_client *client)
{
	std::string 		str;

	if (write(client->fd, (client->team).c_str(), (client->team).size()) == -1)
		throw ConnectError("Write failure");
	str = read_serv(client->fd);
	if (str == "ok\n")
		return 0;
	throw ConnectError("The server cannot handle any more connection for this team");
}

int 					start_com(t_client *client)
{
	fd_set 				fd_read;
	struct timeval		tv;


	FD_ZERO(&fd_read);
	try
	{
		can_connect(client);
	}
	catch (std::exception const &e)
	{
		// std::cout << e.what() << std::endl;
		return -1;
	}
	while (1)
	{
		tv.tv_usec = 10000;
		FD_SET(client->fd, &fd_read);
		if (select(client->fd + 1, &fd_read, NULL, NULL, &tv) == -1)
		{
			// std::cout << "Select failure" << std::endl;
			return -1;
		}
		if (FD_ISSET(client->fd, &fd_read))
		{
			try
			{
				read_serv(client->fd);
			}
			catch (std::exception const &e)
			{
				// std::cout << e.what() << std::endl;
				return -1;
			}
		}
	}
}