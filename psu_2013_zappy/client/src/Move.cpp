/*
** Move.cpp for we in /home/cristi_a/save/zappy_client/src
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Mon Jun  30 12:42:05 2014 toma cristini
** Last update Mon Jun  30 12:42:05 2014 toma cristini
*/
#include "Trantorien.hpp"
#include "exept.hpp"


eAction			Trantorien::move()
{
	// std::cout << CYA << __FUNCTION__ << END << std::endl;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	printDirection();
	// std::cout << "Before pos[" << CYA << position.first << "," << position.second << END << "] ";
	position.first += next_move[direction].first;
	position.second += next_move[direction].second;
	std::cout << GREEN << "\tavance" << END << std::endl;
	// std::cout << "After pos[" << RED << position.first << "," << position.second << END << "]" << std::endl;
	net_.sendMessage("avance");
	return (redirection);
}

eAction			Trantorien::rotateLeft()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << RED << __FUNCTION__ << END << std::endl;
	int x = static_cast<int>(direction) - 1;

	// std::cout << __FUNCTION__ << std::endl;
	if (x < 0)
		x = 3;
	direction = static_cast<eOrientation>(x);
	std::cout << GREEN << "\tgauche" << END << std::endl;
	net_.sendMessage("gauche");
	printDirection();
	return (redirection);
}

eAction			Trantorien::rotateRight()
{
	// std::cout << CYA << __FUNCTION__ << END << std::endl;
	int x = static_cast<int>(direction) + 1;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << __FUNCTION__ << std::endl;
	if (x > 3)
		x = 0;
	direction = static_cast<eOrientation>(x);
	std::cout << GREEN << "\tdroite" << END << std::endl;
	net_.sendMessage("droite");
	printDirection();
	return (redirection);
}

eAction			Trantorien::dirUp()
{
	t_position					dst = chemin.front().first;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << CYA << __FUNCTION__ << END << std::endl;
	if (position.second < dst.second)
		return (MOVE);
	else if (position.first > dst.first)
		return (ROTATE_LEFT);
	else if (position.first < dst.first)
		return (ROTATE_RIGHT);
	return (TURN_BACK);
}

eAction			Trantorien::dirDown()
{
	t_position					dst = chemin.front().first;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << CYA << __FUNCTION__ << END << std::endl;
	if (position.second > dst.second)
		return (MOVE);
	else if (position.first < dst.first)
		return (ROTATE_LEFT);
	else if (position.first > dst.first)
		return (ROTATE_RIGHT);
	return (TURN_BACK);
}

eAction			Trantorien::dirRight()
{
	t_position					dst = chemin.front().first;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << CYA << __FUNCTION__ << END << std::endl;
	if (position.first < dst.first)
		return (MOVE);
	else if (position.second < dst.second)
		return (ROTATE_LEFT);
	else if (position.second > dst.second)
		return (ROTATE_RIGHT);;
	return (TURN_BACK);
}

eAction			Trantorien::dirLeft()
{
	t_position					dst = chemin.front().first;

	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << CYA << __FUNCTION__ << END << std::endl;
	if (position.first > dst.first)
		return (MOVE);
	else if (position.second > dst.second)
		return (ROTATE_LEFT);
	else if (position.second < dst.second)
		return (ROTATE_RIGHT);
	return (TURN_BACK);
}

eAction		Trantorien::turnBack()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	// std::cout << GREEN << "turnBack" << END << std::endl;
 	rotateRight();
	rotateRight();
	return (MOVE);
}

void		Trantorien::printDirection()
{
	switch (direction)
		{
			case UP:
				// std::cout << "Direction [" << MAGENTA << "UP" << END << "]" << std::endl;
				break;
			case DOWN:
				// std::cout << "Direction [" << MAGENTA << "DOWN" << END << "]" << std::endl;
				break;
			case LEFT:
				// std::cout << "Direction [" << MAGENTA << "LEFT" << END << "]" << std::endl;
				break;
			case RIGHT:
				// std::cout << "Direction [" << MAGENTA << "RIGHT" << END << "]" << std::endl;
				break;
		}
}