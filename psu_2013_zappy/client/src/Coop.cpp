/*
** cooperation.cpp for we in /home/cristi_a/rendu/PSU_2013_zappy/client/src
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Fri Jul  04 17:34:44 2014 toma cristini
** Last update Fri Jul  04 17:34:44 2014 toma cristini
*/

#include "Trantorien.hpp"


eAction			Trantorien::teamwork()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	return (VISION);
}

eAction			Trantorien::mobilizArmy()
{
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	if (!vision_incoming)
		{
			c_vision.clear();
			vision_incoming = true;
			position = std::make_pair(0, 0);
			direction = UP;
			std::cout << GREEN << "\tvoir" << END << std::endl;
			net_.sendMessage("voir");
		}
	else if (c_vision.size() != 0)
	{
		std::cout << "\t\tvision received" << std::endl;
		vision_incoming = false;
		if (c_vision[0].second["joueur"] == required[level_]["joueur"])
		{
			for (int i = 0; i < 10; ++i)
				std::cout << "ENOUGH PLAYER TO INCANT" << std::endl;
			return (INCANT);
		}
		else
		{
			std::cout << GREEN << "broadcast " + net_.team_ + " ni " + std::to_string(level_) << END << std::endl;
			net_.sendMessage("broadcast " + net_.team_ + " ni " + std::to_string(level_ + 1));
		}
	}
	return (MOBILIZ_ARMY);
}

eAction			Trantorien::askFriends()
{
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	std::cout << GREEN << "\tconnect_nbr" << END << std::endl;
	net_.sendMessage("connect_nbr");
	if (connect_nb < required[level_]["joueur"])
	{
		std::cout << GREEN << "\tfork" << END << std::endl;
		net_.sendMessage("fork");
	}
	else
	{
		std::cout << GREEN << "\tbroadcast " + net_.team_ + " ni " + std::to_string(level_) << END << std::endl;
	  net_.sendMessage("broadcast " + net_.team_ + " ni " + std::to_string(level_ + 1));
	}
	return (MOBILIZ_ARMY);
}

eAction			Trantorien::joinFriend(int i, const std::string &tmp)
{
	t_inventaire				j;

	// (void)i;
	(void)tmp;
	// if (msg == "swag")
	// 	can_talk = false;
	// else if (msg == "sweg")
	// 	can_talk = true;
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	chemin.erase(chemin.begin(), chemin.begin() + chemin.size());
	chemin.push_back(std::make_pair(getBroadcastDirection(i), j));
	position = std::make_pair(0, 0);
	return (MOVING_TO_YOU);
}

t_position 		Trantorien::getBroadcastDirection(int i)
{
	t_position rslt;

  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	switch (i)
	{
		case 0:
			rslt = std::make_pair(0, 0);
			break;
		case 1:
			rslt = std::make_pair(0, 1);
			break;
		case 2:
			rslt = std::make_pair(-1, 1);
			break;
		case 3:
			rslt = std::make_pair(-1, 0);
			break;
		case 4:
			rslt = std::make_pair(-1, -1);
			break;
		case 5:
			rslt = std::make_pair(0, -1);
			break;
		case 6:
			rslt = std::make_pair(1, -1);
			break;
		case 7:
			rslt = std::make_pair(1, 0);
			break;
		case 8:
			rslt = std::make_pair(1, 1);
			break;
	}
	return (rslt);
}

eAction			Trantorien::movingToYou()
{
	t_position					dst;

  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
  // std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	dst = chemin[0].first;
	if (dst.first == position.first && dst.second == position.second)
		return (IM_THERE);
	redirection = MOVING_TO_YOU;
	return ((this->*instr[call_m[direction]])());
}

eAction			Trantorien::imThere()
{
  std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	if (!vision_incoming)
		{
			c_vision.clear();
			vision_incoming = true;
			position = std::make_pair(0, 0);
			direction = UP;
			std::cout << GREEN << "\tvoir" << END << std::endl;
			net_.sendMessage("voir");
		}
		else if (c_vision.size() != 0)
		{
			vision_incoming = false;
			if (c_vision[0].second["joueur"] > 1)
			{
				std::cout << "Oh I wish i was a punk rocker" << std::endl;
				return (CHECK_STATE);
			}
		}
	return (CHECK_TEAM);
}
