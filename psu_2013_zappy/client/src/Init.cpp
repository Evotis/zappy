/*
** Level.cpp for Level.cpp in /home/cristi_a/rendu/PSU_2013_zappy/client/src
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Wed Jul  02 14:09:05 2014 toma cristini
** Last update Wed Jul  02 14:09:05 2014 toma cristini
*/

#include "Trantorien.hpp"

void 	Trantorien::init()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	initInstr();
	initMsgServ();
	initMsgTeam();
	initInventaire(inventaire);
	initMove();
	level();
}

void 	Trantorien::initInstr()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	instr[CHECK_STATE] = &Trantorien::checkState;
	instr[CHECK_TEAM] = &Trantorien::checkTeam;
	instr[TEAMWORK] = &Trantorien::teamwork;
	instr[VISION] = &Trantorien::vision;
	instr[BUILD_WAY] = &Trantorien::buildWay;
	instr[EXECUTE_WAY] = &Trantorien::executeWay;
	instr[ROTATE_LEFT] = &Trantorien::rotateLeft;
	instr[ROTATE_RIGHT] = &Trantorien::rotateRight;
	instr[MOVE] = &Trantorien::move;
	// instr[TAKE_OBJ] = &Trantorien::takeObj;
	instr[MOVUP] = &Trantorien::dirUp;
	instr[MOVDOWN] = &Trantorien::dirDown;
	instr[MOVLEFT] = &Trantorien::dirLeft;
	instr[MOVRIGHT] = &Trantorien::dirRight;
	instr[TURN_BACK] = &Trantorien::turnBack;
	instr[MOVING_TO_YOU] = &Trantorien::movingToYou;
	instr[IM_THERE] = &Trantorien::imThere;
	// instr[JOINFRIEND] = &Trantorien::joinFriend;
	instr[ASK_FRIENDS] = &Trantorien::askFriends;
	instr[INCANT] = &Trantorien::incant;
	instr[MOBILIZ_ARMY] = &Trantorien::mobilizArmy;
}

void	Trantorien::initMsgServ()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	msg_server["ok"] = &Trantorien::treatConfirmation;
	msg_server["ko"] = &Trantorien::treatFailure;
	msg_server["mort"] = &Trantorien::treatMort;
	msg_server["message [0-8],[a-z1-9 _-]*"] = &Trantorien::treatBroadcast;
	msg_server["deplacement: [0-8]"] = &Trantorien::treatExpulse;
	msg_server["\\{[a-z ,]*\\}"] = &Trantorien::treatVision;
	msg_server["\\{[a-z]* [0-9]+,[a-z]* [0-9]+,[a-z]* [0-9]+,[a-z]* [0-9]+,[a-z]* [0-9]+,[a-z]* [0-9]+,[a-z]* [0-9]+\\}"] = &Trantorien::treatInventory;
	msg_server["[0-9]+"] = &Trantorien::treatConnections;
	msg_server["elevation en cours"] = &Trantorien::treatIncant;
	msg_server["niveau actuel : [1-8]"] = &Trantorien::treatLevel;
}

void		Trantorien::initMsgTeam()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	msg_team[net_.team_ + " ni [2-8]"] = &Trantorien::teamNeedIncant;
	msg_team[net_.team_ + " ri [0-9]* [2-8]"] = &Trantorien::teamReadyIncant;
	msg_team[net_.team_ + " nb [2-8]"] = &Trantorien::teamNb;
	msg_team[net_.team_ + " co-nb [0-9]*"] = &Trantorien::teamCoNb;
	msg_team[net_.team_ + " nwp"] = &Trantorien::teamNewPlayer;
}

void Trantorien::initInventaire(t_inventaire &i)
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	i["nourriture"] = 0;
	i["linemate"] = 0;
	i["deraumere"] = 0;
	i["sibur"] = 0;
	i["mendiane"] = 0;
	i["phiras"] = 0;
	i["thystame"] = 0;
}

void	Trantorien::initMove()
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	call_m[UP] = MOVUP;
	call_m[DOWN] = MOVDOWN;
	call_m[LEFT] = MOVLEFT;
	call_m[RIGHT] = MOVRIGHT;

	direction = UP;
	next_move[UP] = std::make_pair(0, 1);
	next_move[DOWN] = std::make_pair(0, -1);
	next_move[RIGHT] = std::make_pair(1, 0);
	next_move[LEFT] = std::make_pair(-1, 0);

	position = std::make_pair(0, 0);

	pause_incant = false;
	vision_incoming = false;

	can_talk = true;


	team_id = 0;
	connect_nb = 1;
	inventaire["joueur"] = 1;
}