/*
** trantorien.cpp for we in /home/cristi_a/save/stand
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Sat Jun  21 17:05:29 2014 toma cristini
** Last update Sat Jun  21 17:05:29 2014 toma cristini
*/

#include <unistd.h>
#include "Trantorien.hpp"

Trantorien::Trantorien(t_param &param) : lifetime(1260), level_(1), alive_(true), state(OCCUPE)
{
	net_.team_ = param.team;
	init();
  net_.initConnection(param);
  net_.garantAccess();
}

Trantorien::~Trantorien()
{

}

Trantorien::Trantorien(Trantorien const & nt)
{
	// team = nt.getTeam();
	(void)nt;
}

Trantorien& Trantorien::operator=(const Trantorien& nt)
{
	(void)nt;
	// if (this != &nt)
	// {
	// 	team = nt.getTeam();
	// }

	return *this;
}

bool 		Trantorien::start()
{
	eAction 	next_action = CHECK_STATE;
	// net_.sendMessage("inventaire");
	net_.sendMessage("broadcast " + net_.team_ + " nwp");
	while (alive_)
	{
		next_action = (this->*instr[next_action])();
		//// std::cout << "Lifetime [" << lifetime << "]" << std::endl;
		//lifetime--;
		usleep(500000);
		checkTeam();
		treatServer();
	}
	return true;
}

t_position 	Trantorien::getPosition(int i)
{
	std::cout << MAGENTA << __FUNCTION__ << END << std::endl;
	std::pair<int, int> rslt;
	int nb_case = 3;
	int nb_up = 1;
	// int nb_left = 0;
	int nb_right = 0;

	// std::cout << "\tgetPosition(" << i << ") ";
	if (i == 0)
		return (std::make_pair(0, 0));
	while (i > nb_case)
	{
		i -= nb_case;
		nb_case = nb_case + 2;
		nb_up++;
	}
	nb_right = i - (1 + (nb_case / 2));
	rslt.first = nb_right;
	rslt.second = nb_up;
	return (rslt);
}

void		Trantorien::treatServer() {
	net_.checkServ();

	t_comList	&serv_m = net_.getInCommunication();


	// // std::cout << "in server_queue" << std::endl;
	// for (t_comList::iterator i = serv_m.begin(); i != serv_m.end(); ++i)
	// {
	// 	// std::cout << "\t"<<  *i << std::endl;
	// }

	while (!serv_m.empty()) {
		treatMessage(serv_m.front());
		serv_m.pop_front();
	}
}

void		Trantorien::treatMessage(std::string const &in) {

	// std::cout << MAGENTA << "\t\t" << in << END << std::endl;
	for (t_server_treat::iterator i = msg_server.begin(); i != msg_server.end(); ++i) {
		if (boost::regex_match(in, boost::regex(i->first))) {
			// std::cout << RED << "REGEX MATCH " << i->first << " with " << in << END << std::endl;
			(this->*(i->second))(in);
		}
	}
}