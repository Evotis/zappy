/*
** main.c for main in /home/fest/project/sysu/zappy/projet/client_dir
**
** Made by Adrien Chataignoux
** Login   <chatai_b@epitech.eu>
**
** Started on  Sun May 25 15:49:10 2014 Adrien Chataignoux
** Last update Wed Jun 18 18:12:47 2014 Adrien Chataignoux
*/

#include <unistd.h>
#include <sstream>
#include "Trantorien.hpp"
#include "network/Network.hpp"
// #include "client.h"

bool                getParams(int ac, char **av, t_param &param) {
  std::stringstream ss;
  int               opt;

  while ((opt = getopt(ac, av, "n:p:h:")) != -1)
  {
    switch (opt)
    {
     case 'n':
     param.team = optarg;
     break;
     case 'p':
     ss << optarg;
     ss >> param.port;
     break;
     case 'h':
     param.machine = optarg;
     break;
     default:
     return (false);
     break;
   }
 }
 return (true);
}

int                  main(int ac, char **av)
{
  t_param           param;

  param.port = 4242;
  param.machine = "127.0.0.1";
  if (ac < 3 || !getParams(ac, av, param)) {
    std::cerr << "Usage : ./client -n Team_name -p Port [-h Machine name]" << std::endl;
    return (1);
  }
 if (param.team.empty())
  {
    std::cerr << "Usage : ./client -n Team_name (-p [Port]) (-h [Machine name])" << std::endl;
    return (0);
  }
  Trantorien  tomi(param);
  tomi.start();
  return (0);
}
