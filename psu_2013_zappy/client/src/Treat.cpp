/*
** Treat.cpp for src in /home/mohame_e/project/SysUnix/PSU_2013_zappy/client/src
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Tue Jul 01 13:28:38 2014 Ahmed Mohamed Ali
** Last update Tue Jul 01 13:28:38 2014 Ahmed Mohamed Ali
*/

#include "Trantorien.hpp"

void      Trantorien::treatConfirmation(std::string const &in) {
  std::size_t  x;
  std::string &out = net_.getOutCommunication().front();

  // std::cout << YELLOW << out << END << std::endl;
  (void)in;
  if ((x = out.find("prend")) != out.npos) {
    std::string   str = out.substr(6);
    // std::cout << CYA << "\tConfirmation take on [" << str << "]" << END << std::endl;
    inventaire[str] += 1;
  }
  else if ((x = out.find("pose")) != out.npos) {
    std::string   str = out.substr(5);
    inventaire[str] -= 1;
  }
  else if (out == "fork") {
    pause_incant = false;
  }
  if (!(net_.getOutCommunication().empty()))
    net_.getOutCommunication().pop_front();
}

void      Trantorien::treatFailure(std::string const &in) {
  std::size_t  x;
  std::string &out = net_.getOutCommunication().front();

  (void)in;
  if ((x = out.find("pose")) != out.npos) {
    std::string   str = out.substr(5);
    // std::cout << RED << "\tFail pose : " << str << END << std::endl;
    inventaire[str] = 0;
  }
  else if (out == "incantation")
  {
    // std::cout << RED << "\tFail Incant" << END << std::endl;
    pause_incant = false;
  }
  else if ((x = out.find("prend")) != out.npos)
  {
    std::string   str = out.substr(6);
    // std::cout << RED << "\tFail prend :" << str << END << std::endl;
    pause_incant = false;
  }
  // std::cout << out << std::endl;
  if (!(net_.getOutCommunication().empty()))
    net_.getOutCommunication().pop_front();
}

void      Trantorien::treatMort(std::string const &in) {
  (void)in;
  // std::cout << "TOMI EST MORT" << std::endl;
  alive_ = false;
}

void      Trantorien::treatBroadcast(std::string const &in) {
  std::stringstream ss;
  int   from;

  std::string val = in.substr(7);
  std::string msg = val.substr(3);

  ss << val;
  ss >> from;
  joinFriend(from, msg);
  // std::cout << msg << "[" << from << "]" << std::endl;
//   joinFriend(from);
  in_team_msg.push_back(std::make_pair(from, msg));
}

void      Trantorien::treatExpulse(std::string const &in) {
  std::stringstream ss;
  std::string       val;
  int   from;

  val = in.substr(12);
  ss << in;
  ss >> from;
}

void            Trantorien::treatVision(std::string const &in) {
  int           unit = 0;
  int           i;
  std::size_t   x = 0;
  std::size_t   z = 0;
  std::string   s = in.substr(1, in.size() - 2);
  std::string   val;
  std::string   &out = net_.getOutCommunication().front();
  std::string   tab[8] = {"nourriture", "linemate", "deraumere", "sibur", "mendiane", "phiras", "thystame", "joueur"};
  t_inventaire         phil;

  // std::cout << GREEN << out << END << std::endl;
  if (out != "voir")
    return ;
  c_vision.clear();
  while ((x = s.find(',')) != s.npos) {
    val = s.substr(0, x);
    // std::cout << "[" << val << "] " << val.size() << std::endl;
    initInventaire(phil);
    i = 0;
    while (i < 8) {
      z = 0;
      while ((z = val.find(tab[i], z + 1)) != val.npos)
        phil[tab[i]] += 1;
      ++i;
    }
    c_vision.push_back(std::make_pair(unit, phil));
    s = s.substr(x + 1);
    unit += 1;
  }
  c_vision.front().second["joueur"] -= 1;
  // std::cout << "[" << s << "] " << s.size() << std::endl;
  i = 0;
  initInventaire(phil);
  while (i < 8) {
    z = 0;
    while ((z = s.find(tab[i], z + 1)) != s.npos)
      phil[tab[i]] += 1;
    ++i;
  }
  c_vision.push_back(std::make_pair(unit, phil));
  if (!(net_.getOutCommunication().empty()))
    net_.getOutCommunication().pop_front();
}

void      Trantorien::treatInventory(std::string const &in) {
  int     i = 0;
  int     tmp;
  std::size_t x;
  std::size_t y;
  std::size_t z;
  std::string tab[7] = {"nourriture", "linemate", "deraumere", "sibur", "mendiane", "phiras", "thystame"};
  std::string     s = in.substr(1, in.size() - 3);
  std::string     str;
  std::stringstream ss;
  std::string &out = net_.getOutCommunication().front();

  // std::cout << YELLOW << out << END << std::endl;
  if (out != "inventaire")
    return ;
  while (i < 7) {
    tmp = 0;
    x = s.find(tab[i]);
    y = s.find(',', x);
    if (y != s.npos)
      str = s.substr(x, y - x);
    z = str.find(' ');
    str = str.substr(z + 1);
    ss << str;
    ss >> tmp;
    ss.clear();
    inventaire[tab[i]] = tmp;
    ++i;
  }
  if (!(net_.getOutCommunication().empty()))
    net_.getOutCommunication().pop_front();
}

void      Trantorien::treatConnections(std::string const &in) {
  std::stringstream ss;

  ss << in;
  ss >> net_.clientsLeft_;
  if (!(net_.getOutCommunication().empty()))
    net_.getOutCommunication().pop_front();
}

void      Trantorien::treatIncant(std::string const &in) {
  (void)in;
  pause_incant = true;
  if (!(net_.getOutCommunication().empty()))
    net_.getOutCommunication().pop_front();
}

void      Trantorien::treatLevel(std::string const &in) {
  pause_incant = false;
  std::stringstream ss;
  ss << in.substr(16);
  ss >> level_;
}
