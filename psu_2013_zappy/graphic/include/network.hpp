//
// network.hpp for graphic_client in /home/eyzat_f/projets/psu/zappy/graphic
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Mon Jun 23 15:24:48 2014 florian eyzat
// Last update Tue Jul  8 20:00:08 2014 florian eyzat
//

#ifndef NETWORK_HPP_
# define NETWORK_HPP_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>

class			Network
{
  int			sockfd;
  fd_set		readfds;
  struct sockaddr_in	server;

  public:

  Network();
  ~Network();

  bool			connectToServ(char *, int);
  const char		*recupInfo();
  const char		*receive_it();
};

#endif
