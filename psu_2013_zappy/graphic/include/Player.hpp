//
// Player.hpp for graphic_client in /home/eyzat_f/projets/psu/zappy/psu_2013_zappy/2d_client
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Tue Jul  1 12:47:05 2014 florian eyzat
// Last update Sun Jul 13 21:06:14 2014 florian eyzat
//

#include "Map.hpp"

class	Player
{
 private:
  int	x;
  int	y;

  bool	incantation;

  unsigned short	nbFood;
  unsigned short	nbLinemate;
  unsigned short	nbDeraumere;
  unsigned short	nbSibur;
  unsigned short	nbMendiane;
  unsigned short	nbPhiras;
  unsigned short	nbThystame;
  orient		orientation;
  std::vector<bool>	eggs;
  std::string		team;
  unsigned int		value;

 public:
  bool			alive;
  Player(int, int, std::string, unsigned int _value);
  ~Player();

  unsigned short	getFood() const ;
  unsigned short	getLinemate() const ;
  unsigned short	getDeraumere() const ;
  unsigned short	getSibur() const ;
  unsigned short	getMendiane() const ;
  unsigned short	getPhiras() const ;
  unsigned short	getThystame() const ;
  bool			getIncantation() const ;
  orient		getOrientation() const ;
  int			getX() const ;
  int			getY() const ;
  std::string		getTeamName() const;
  int			getValue() const ;

  void	setFood(unsigned short);
  void	setLinemate(unsigned short);
  void	setDeraumere(unsigned short);
  void	setSibur(unsigned short);
  void	setMendiane(unsigned short);
  void	setPhiras(unsigned short);
  void	setThystame(unsigned short);
  void	setIncantation(bool);
  void	setOrientation(orient);
  void	setX(int);
  void	setY(int);

  void	addEgg();
  
  void	putEgg(); // put an egg on the map
  void	getThing(element); // take the thing off the map to place it in the inventory
  void	throwThing(element); // take the thing off the inventory to place it in the map
};

extern std::vector<Player *>	g_players;
