//
// Map.hpp for graphic_client in /home/eyzat_f/projets/psu/zappy/psu_2013_zappy/2d_client
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Tue Jul  1 14:59:58 2014 florian eyzat
// Last update Sun Jul 13 20:47:00 2014 florian eyzat
//

#ifndef		MAP_HPP_
# define	MAP_HPP_

#include <iostream>
#include <vector>
#include <list>

typedef std::vector<std::vector<std::vector<int> > > my_map;

extern my_map _map;

typedef	enum
  {
    FOOD,
    LINEMATE,
    DERAUMERE,
    SIBUR,
    MENDIANE,
    PHIRAS,
    THYSMANE
  }	element;

typedef	enum
  {
    UP,
    RIGHT,
    DOWN,
    LEFT
  }	orient;

void		recup_case(int, int, std::vector<int> *); // index dans l'ordre
void		modif_case(int, int, std::vector<int> *); //index dans l'ordre
bool		isMapEmpty(int, int);

#endif
