#ifndef SERVER_COM_HH_
#define SERVER_COM_HH_

#include <iostream>
#include <map>
#include "Player.hpp"

class ServerCom
{
public:
	ServerCom();
	~ServerCom();
	ServerCom(ServerCom const &);
	ServerCom &operator=(ServerCom const &);

	void	player_inventory(std::string const &);
	void	player_incant(std::string const &);
	void	player_egg(std::string const &);
	void	new_player(std::string const &);
	void	player_pos(std::string const &);
	void	player_level(std::string const &);
	void	expulsion(std::string const &);
	void	player_starvation(std::string const &);
	void	player_broadcast(std::string const &);
	void	player_incant_end(std::string const &);
	void	player_drop(std::string const &);
	void	player_take(std::string const &);
	void	egg_laid(std::string const &);
	void	egg_birth(std::string const &);
	void	egg_connect(std::string const &);
	void	egg_die(std::string const &);
	void	time_ask(std::string const &);
	void	map_case(std::string const &);
	void	map_size(std::string const &);
	void	team_names(std::string const &);
  void	com(std::string const &);
private:
	std::map<std::string, void (ServerCom::*)(std::string const &)> instr;
};

int             	cut(std::string line, int x, int y);

extern int max_x;
extern int max_y;

#endif
