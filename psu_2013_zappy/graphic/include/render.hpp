//
// render.hpp for graphic_client in /home/eyzat_f/projets/psu/zappy/graphic/include
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Wed Jun 18 16:18:12 2014 florian eyzat
// Last update Sun Jul 13 21:04:03 2014 florian eyzat
//

#ifndef		GRAPHIC_HPP_
# define	GRAPHIC_HPP_

#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <map>
#include <SFML/Graphics.hpp>
#include <unistd.h>

#include "ServerCom.hpp"
#include "network.hpp"

std::string		return_str(int);

typedef std::vector<std::vector<std::vector<int > > > my_map;

extern const int nbElem;
extern sf::Sprite infos;
extern sf::Sprite border;
extern int	MouseX;
extern int	MouseY;
extern unsigned int	marker;

typedef struct		s_textures
{
  sf::Sprite		texture;
  char			ind;
}			t_textures;

class Render
{
private:

  int			_lenX;
  int			_lenY;
  int			minX;
  int			maxX;
  int			minY;
  int			maxY;
  int			mult;

  std::vector<t_textures>	graph;
  ///////////////
  // SFML
  ///////////////
  sf::RenderWindow	app;
  ///////////////
  my_map		map;
  int				NB_TEXTURES;

  int		posX;
  int		posY;
  int		refX;
  int		refY;

  bool		endTurn;
  bool		scroll;

  sf::Sprite	persoUP;
  sf::Sprite	persoDOWN;
  sf::Sprite	persoLEFT;
  sf::Sprite	persoRIGHT;
  sf::Sprite	incant;

public:
  Render();
  ~Render();

  //
  //basics
  //
  bool			start(Network &, char *&, int);
  bool			start_aff(Network &, char *&, int);
  bool			aff_thing(int, int, int, int, int);
  void			aff_this(unsigned int);
  bool			routine();
  void			edit_game();
  int			get_thing(std::vector<int> &) const;

  //
  //map part
  //
  bool			aff_elem(int, int, bool);
  bool			aff_map();
  void			aff_players();
  void			aff_infos();
  char			get_char(int, int) const;
  void			fill_map();
  void			moveMap();
  bool			gereEvent();
  void			aff_place();
};

#endif /* !GRAPHIC_HPP_ */
