//
// Egg.hpp for graphic_client in /home/eyzat_f/projets/psu/zappy/psu_2013_zappy/2d_client
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Fri Jul  4 12:50:18 2014 florian eyzat
// Last update Fri Jul  4 12:55:54 2014 florian eyzat
//

#include "Map.hpp"

class	Egg
{
private:
  int	x;
  int	y;
  bool	alive;

public:
  Egg(int, int);
  ~Egg();

  int	getX() const;
  int	getY() const;

  void	setX(int);
  void	setY(int);

  bool	isTurned() const; // returns false if it's still an egg
};

extern std::vector<Egg *>	g_egg;
