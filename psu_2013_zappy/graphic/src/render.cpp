//
// render.cpp for graphic_client in /home/eyzat_f/projets/psu/zappy/graphic
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Wed Jun 18 16:32:43 2014 florian eyzat
// Last update Sun Jul 13 21:29:26 2014 florian eyzat
//

#include "render.hpp"

const int nbElem = 3;

Render::Render()
  : _lenX(0), _lenY(0), graph(10), app(sf::VideoMode(1200, 700, 32), "Zappy Graphic Client")
{
  max_x = 0;
  max_y = 0;
  NB_TEXTURES = 6;
  mult = 50;

  scroll = false;
  posX = 0;
  posY = 0;
}

Render::~Render()
{
}

sf::Sprite	infos;
sf::Sprite	border;

bool		Render::start(Network &net, char *&ip, int port)
{
  sf::Image	loading;
  sf::Image	image0;
  sf::Image	image1;
  sf::Image	image2;
  sf::Image	image3;
  sf::Image	image4;
  sf::Image	image5;
  sf::Image	image6;
  sf::Image	image7;
  sf::Image	image8;
  sf::Image	image9;
  sf::Image	inf;
  sf::Image	bord;
  sf::Image	perso1;
  sf::Image	perso2;
  sf::Image	perso3;
  sf::Image	perso4;
  sf::Image	incantation;

  //////////
  // init //
  //////////

  sf::Sprite	load;

  if (!loading.LoadFromFile("textures/loading.png"))
    return (false);
  load.SetImage(loading);
  app.Clear();
  app.Draw(load);
  app.Display();

  if (!inf.LoadFromFile("textures/infos.png"))
    return (false);
  infos.SetImage(inf);

  if (!bord.LoadFromFile("textures/border.png"))
    return (false);
  border.SetImage(bord);

  if (!image0.LoadFromFile("textures/burger2.png"))
    return (false);
  graph[0].texture.SetImage(image0);

  if (!image1.LoadFromFile("textures/blueR.png"))
    return (false);
  graph[1].texture.SetImage(image1);

  if (!image2.LoadFromFile("textures/silverR.png"))
    return (false);
  graph[2].texture.SetImage(image2);

  if (!image3.LoadFromFile("textures/goldR.png"))
    return (false);
  graph[3].texture.SetImage(image3);

  if (!image4.LoadFromFile("textures/redR.png"))
    return (false);
  graph[4].texture.SetImage(image4);

  if (!image5.LoadFromFile("textures/GSR.png"))
    return (false);
  graph[5].texture.SetImage(image5);

  if (!image6.LoadFromFile("textures/greenR.png"))
    return (false);
  graph[6].texture.SetImage(image6);

  if (!image7.LoadFromFile("textures/green4.png"))
    return (false);
  graph[7].texture.SetImage(image7);

  if (!image8.LoadFromFile("textures/green3.png"))
    return (false);
  graph[8].texture.SetImage(image8);

  if (!image9.LoadFromFile("textures/egg.png"))
    return (false);
  graph[9].texture.SetImage(image9);

  if (!perso1.LoadFromFile("textures/pikachuAVANT.png"))
    return (false);
  persoDOWN.SetImage(perso1);

  if (!perso2.LoadFromFile("textures/pikachuARRIERE.png"))
    return (false);
  persoUP.SetImage(perso2);

  if (!perso3.LoadFromFile("textures/pikachuDROITE.png"))
    return (false);
  persoRIGHT.SetImage(perso3);

  if (!perso4.LoadFromFile("textures/pikachuGAUCHE.png"))
    return (false);
  persoLEFT.SetImage(perso4);

  if (!incantation.LoadFromFile("textures/incantation.png"))
    return (false);
  incant.SetImage(incantation);

  return (start_aff(net, ip, port));
}

///////////////
//           //
//  routine  // 
//           //
///////////////

void	Render::edit_game()
{
  if (_lenX != max_y || _lenY != max_x) {
    _lenX = max_y;
    _lenY = max_x;
    if (_lenX > 15 && _lenY > 15)
      {
	refX = 800 / mult;
	refY = 700 / mult;
	scroll = true;
      }
    else {
      refX = 2;
      refY = 2;
    }
  }
}

bool	Render::start_aff(Network &net, char *&ip, int port)
{
  ServerCom	communication;
  bool		exit;
  bool		connect;
  std::string	comm;
  char		*str;
  int		aff_mode = 0;

  exit = true;
  if ((connect = net.connectToServ(ip, port)) == false)
    return (false);
  while (exit) {
    while (exit && (str = (char *)(net.recupInfo())))
      {
	aff_mode += 1;
	if (gereEvent() == false)
	  return (false);
	std::cerr << str;
	if (str) {
	  if ((comm = str) == "CLOSE\n") {
	    return (false);
	  }
	  communication.com(static_cast<std::string const>(comm));
	}
	edit_game();
	if ((max_x > 100 && max_y > 100) || max_x > 150 || max_y > 150) {
	  std::cerr << "error with the map" << std::endl;
	  return (false);
	}
	if (aff_mode > 5 && max_x && max_y) {
	  exit = routine();
	  aff_mode = 0;
	}
      }
    if (exit == false)
      return (false);
    if (str == NULL) {
      if (gereEvent() == false)
	return (false);
      aff_mode += 1;
    }
    if (gereEvent() == false)
      return (false);
    if (aff_mode > 5 && max_x && max_y) {
      aff_mode = 5;
      exit = routine();
    }
  }
  return (true);
}

bool		Render::routine()
{
  if (!app.IsOpened())
    return (false);
  app.Clear(sf::Color(255, 228, 196));
  aff_map();
  app.Display();
  return (true);
}


  ////////////////////////////////////
  //            display             //
  ////////////////////////////////////

bool		Render::aff_thing(int index, int x, int y, int decal, int decaly)
{
  graph[index].texture.SetPosition((x * mult) + decal, (y * mult) + decaly);
  app.Draw(graph[index].texture);
  return (true);
}


  //////////////////////////////////////
  //              events              //
  //////////////////////////////////////
int		MouseX = -1;
int		MouseY = -1;

bool		Render::gereEvent()
{
  sf::Event	event;

  while (app.GetEvent(event))
    {
      if (event.Type == sf::Event::Closed)
	return (false);
      if((event.Type == sf::Event::MouseButtonPressed) && (event.MouseButton.Button == sf::Mouse::Left))
	{
	  marker = 0;
	  MouseX = app.GetInput().GetMouseX();
	  MouseY = app.GetInput().GetMouseY();
	}
      switch (event.Key.Code)
	{
	case sf::Key::Escape : {
	  return (false);
	}
	case sf::Key::Right : {
	  if (posX < _lenX - refX && _lenX > 15)
	    posX += 1;
	  MouseX = -1;
	  MouseY = -1;
	  break;
	}
	case sf::Key::Left : {
	  if (posX > 0 && _lenX > 15)
	    posX -= 1;
	  MouseX = -1;
	  MouseY = -1;
	  break;
	}
	case sf::Key::Up : {
	  if (posY > 0 && _lenY > 15)
	    posY -= 1;
	  MouseX = -1;
	  MouseY = -1;
	  break;
	}
	case sf::Key::Down : {
	  if (posY < _lenY - refY && _lenY > 15)
	    posY += 1;
	  MouseX = -1;
	  MouseY = -1;
	}
	default : break;
	}
    }
  return (true);
}
