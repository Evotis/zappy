//
// map.cpp for graphic_client in /home/eyzat_f/projets/psu/zappy/graphic
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Wed Jun 18 16:24:46 2014 florian eyzat
// Last update Sun Jul 13 21:06:31 2014 florian eyzat
//

#include <sstream>
#include "render.hpp"


my_map		_map;


void	recup_case(int x, int y, std::vector<int> *final)
{
  *final = _map[y][x];
}

void		modif_case(int x, int y, std::vector<int> *final)
{
  _map[y][x] = *final;
}

//////////////////////////
//map part of the render//
//////////////////////////

void		Render::fill_map()
{
  int		i;
  int		j;

  srand(time(NULL));

  std::cout << "=========================" << std::endl;
  for (i = 0; i < _lenX; ++i){
    for (j = 0; j < _lenY; ++j){      
      std::vector<int>	vec;

      vec.push_back(rand() % 4);
      vec.push_back(rand() % 4);
      vec.push_back(rand() % 4);
      vec.push_back(rand() % 4);
      vec.push_back(rand() % 4);
      vec.push_back(rand() % 4);
      vec.push_back(rand() % 4);
      map[i][j] = vec;
    }
  }
  std::cout << "=========================" << std::endl;
}

bool		Render::aff_elem(int x, int y, bool)
{
  int		i;
  int		decX = 0;
  int		decY = 0;
  int		grass = 7;

  if (isMapEmpty(y, x))
    grass = 8;

  aff_thing(grass, x - posX, y - posY, 0, 0);
  for (i = 0; i < 7; ++i)
    {
      if (static_cast<unsigned int>(y) < _map.size()
	  && static_cast<unsigned int>(x) < _map[y].size()
	  && _map[y][x][i]) {
	aff_thing(i, x - posX, y - posY, decX, decY);
      }
      decX += 10;
      if (decX == 40)
	{
	  decX = 0;
	  decY += 15;
	}
    }
  return (true);
}

void		Render::aff_players()
{
  int		i = 0;

  if (g_players.size()) {
    for (i = 0; i < static_cast<int>(g_players.size()); ++i) {

      if (g_players[i] && g_players[i]->alive)
	{
	  if (g_players[i]->getX() >= posX && g_players[i]->getX() < (posX + refX)
	      && g_players[i]->getY() >= posY && g_players[i]->getY() < (posY + refY))
	    {
	      if (g_players[i]->getIncantation() == true)
		{
		  incant.SetPosition((g_players[i]->getX() - posX) * mult,
				     (g_players[i]->getY() - posY) * mult);
		}
	      else if (g_players[i]->getOrientation() == UP) {
		persoUP.SetPosition((g_players[i]->getX() - posX) * mult,
				    (g_players[i]->getY() - posY) * mult);
		app.Draw(persoUP);
	      }	    
	      else if (g_players[i]->getOrientation() == DOWN) {
		persoDOWN.SetPosition((g_players[i]->getX() - posX) * mult,
				      (g_players[i]->getY() - posY) * mult);
		app.Draw(persoDOWN);
	      }	    
	      else if (g_players[i]->getOrientation() == LEFT) {
		persoLEFT.SetPosition((g_players[i]->getX() - posX) * mult,
				      (g_players[i]->getY() - posY) * mult);
		app.Draw(persoLEFT);
	      }	    
	      else if (g_players[i]->getOrientation() == RIGHT) {
		persoRIGHT.SetPosition((g_players[i]->getX() - posX) * mult,
				       (g_players[i]->getY() - posY) * mult);
		app.Draw(persoRIGHT);
	      }	    
	    }
	}
    }
  }
}


bool		Render::aff_map()
{
  int		i;
  int		j;

  endTurn = true;
  for (i = posX; i < _lenX; ++i){
    for (j = posY; j < _lenY; ++j) {
      aff_elem(i, j, scroll);
    }
  }
  aff_players();
  aff_infos();
  for ( i = 0; i < 14; ++i) {
    border.SetPosition(800, i * mult);
    app.Draw(border);
  }
  return (true);
}
