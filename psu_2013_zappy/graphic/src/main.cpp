//
// main.cpp for graphic_client in /home/eyzat_f/projets/psu/zappy/graphic
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Wed Jun 18 17:11:22 2014 florian eyzat
// Last update Sun Jul 13 21:27:32 2014 florian eyzat
//

#include "render.hpp"
#include "network.hpp"

int     max_x;
int     max_y;

int		main(int argc, char **argv)
{
  Render	aff;
  Network	net;

  static_cast<void>(argc);

  if (argv[1] && argv[2])
    {
      aff.start(net, argv[1], atoi(argv[2]));
    }
  else
    std::cerr << "Usage : [IP] [PORT]" << std::endl;
  return (0);
}
