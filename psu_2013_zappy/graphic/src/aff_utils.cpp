//
// aff_utils.cpp for aff_utils in /home/eyzat_f/projets/psu/zappy/psu_2013_zappy/2d_client
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Sun Jul 13 20:41:17 2014 florian eyzat
// Last update Sun Jul 13 21:49:23 2014 florian eyzat
//

#include <sstream>
#include "render.hpp"

std::string	aff_nb = "Player nb ";
std::string	aff_inv = "\n\nInventory : ";
std::string	aff_1 = "\n\n\nfood      -> ";
std::string	aff_2 = "\n\nlinemate  -> ";
std::string	aff_3 = "\n\nderaumere -> ";
std::string	aff_4 = "\n\nsibur     -> ";
std::string	aff_5 = "\n\nmendiane  -> ";
std::string	aff_6 = "\n\nphiras    -> ";
std::string	aff_7 = "\n\nthystame  -> ";
std::string	aff_x = "\n\nposition x = ";
std::string	aff_y = "\n\nposition y = ";
sf::Font	police;
bool		first = true;
unsigned int	marker = 0;

bool		isMapEmpty(int x, int y)
{
  unsigned int		i;

  i = 0;
  if (static_cast<unsigned int>(x) < _map.size()
      && static_cast<unsigned int>(y) < _map[x].size())
    while (i < _map[x][y].size())
      {
	if (_map[x][y][i] != 0)
	  return (false);
	i += 1;
      }
  return (true);
}

std::string	return_str(int value)
{
  std::ostringstream oss;

  oss << value;
  return (oss.str());
}

void		Render::aff_this(unsigned int i)
{
  sf::String	in("", police, 24);
  std::string	information;
  std::string	orien;

  marker = i;
  if (g_players[i]->getOrientation() == LEFT)
    orien = "left";
  else if (g_players[i]->getOrientation() == RIGHT)
    orien = "right";
  else if (g_players[i]->getOrientation() == UP)
    orien = "up";
  else if (g_players[i]->getOrientation() == DOWN)
    orien = "down";
  information = aff_nb + return_str(g_players[i]->getValue()) + aff_x + return_str(g_players[i]->getX())
    + aff_y + return_str(g_players[i]-> getY()) + "\n\nOrientation = " + orien + aff_inv
    + aff_1 + return_str(g_players[i]->getFood()) + aff_2 + return_str(g_players[i]->getLinemate())
    + aff_3 + return_str(g_players[i]->getDeraumere())+ aff_4 + return_str(g_players[i]->getSibur())
    + aff_5 + return_str(g_players[i]->getMendiane()) + aff_6 + return_str(g_players[i]->getPhiras())
    + aff_7 + return_str(g_players[i]->getThystame());
  in.SetColor(sf::Color(0, 0, 0));
  in.SetText(information);
  in.SetPosition(820, 20);
  app.Draw(in);
}

void		Render::aff_infos()
{
  unsigned int		i;
  int			tmpx;
  int			tmpy;
  bool			in_player = false;
  static bool	aff_ok = true;

  infos.SetPosition(800, 0);
  app.Draw(infos);
  if (first == true)
    {
      first = false;
      if (!police.LoadFromFile("textures/police.ttf"))
  	aff_ok = false;
    }
  if (marker)
    for(i = 0; i < g_players.size(); ++i) {
      if (aff_ok && g_players[i] && g_players[i]->alive) {
	in_player = true;
	aff_this(i);
      }
    }
  else if (MouseX >= 0 && MouseY >= 0)
    {
      for(i = 0; i < g_players.size(); ++i)
	{
	  tmpx = g_players[i]->getX() - posX;
	  tmpy = g_players[i]->getY() - posY;
	  if (MouseX >= tmpx * mult && MouseX <= tmpx * mult + 50
	      && MouseY >= tmpy * mult && MouseY <= tmpy * mult + 50) {
	    if (aff_ok && g_players[i] && g_players[i]->alive) {
	      in_player = true;
	      aff_this(i);
	    }
	  }
	  else
	    if (aff_ok && marker > 0 && marker < g_players.size() && g_players[marker]->alive)
	      aff_this(marker);
	}
    }
  if (!in_player)
    aff_place();
}

void	Render::aff_place()
{
  sf::String	in("", police, 24);
  std::string	information;
  static bool	aff_ok = true;

  if (first == true)
    {
      first = false;
      if (!police.LoadFromFile("textures/police.ttf"))
  	aff_ok = false;
    }
  if (!aff_ok)
    return ;
  information = "Position in the map :\n\n x = " + return_str(posX) + "\n y = " + return_str(posY);
  information += "\n\nContent:";
  if (isMapEmpty(posX, posY)) {
    information += " empty";
    information += "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nref : point in the top / left";
  }
  else {
    information += aff_1 + return_str(_map[posX][posY][0]) + aff_2 + return_str(_map[posX][posY][1])
      + aff_3 + return_str(_map[posX][posY][2])+ aff_4 + return_str(_map[posX][posY][3])
      + aff_5 + return_str(_map[posX][posY][4]) + aff_6 + return_str(_map[posX][posY][5])
      + aff_7 + return_str(_map[posX][posY][6]);
    information += "\n\n\nref : point in the top / left";
  }
  information += "\n\nmap size : x = " + return_str(_lenX) + " y = " + return_str(_lenY);
  information += "\n\nnb players : " + return_str(static_cast<int>(g_players.size()));
  in.SetColor(sf::Color(0, 0, 0));
  in.SetText(information);
  in.SetPosition(820, 20);
  app.Draw(in);
}
