#include <vector>
#include "ServerCom.hpp"

void 					ServerCom::player_level(std::string const &instruction)
{
  int          		i = 4;
  int           		y = 4;
  std::vector<int>	values;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;			
    }
  values.push_back(cut(instruction, y, instruction.length()));
}

void 								ServerCom::player_pos(std::string const &instruction)
{
  int  			        		i = 4;
  int           					y = 4;
  std::vector<int>				values;
  std::vector<Player *>::iterator it;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;			
    }
  values.push_back(cut(instruction, y, instruction.length()));
  if (values[1] >= max_x || values[2] >= max_y)
    return;
  i = 0;
  while (static_cast<unsigned int>(i) < g_players.size()
	 && g_players[i]->getValue() != values[0] && g_players[i])
    i ++;
  if (static_cast<unsigned int>(i) >= g_players.size()
      || (static_cast<unsigned int>(i) < g_players.size() 
	  && g_players[i]->getValue() != values[0]))
    return;
  g_players[i]->setX(values[1] % max_x);
  g_players[i]->setY(values[2] % max_y);
  switch (values[3])
    {
    case 1:
      g_players[i]->setOrientation(UP);
      break;
    case 2:
      g_players[i]->setOrientation(RIGHT);
      break;
    case 3:
      g_players[i]->setOrientation(DOWN);
      break;
    case 4:
      g_players[i]->setOrientation(LEFT);
      break;		
    }
}

void 								ServerCom::new_player(std::string const &instruction)
{
  int          					i = 4;
  int           					y = 4;
  std::vector<int>				values;
  std::string 					team;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;
    }
  team = instruction.substr(y, instruction.length());
  if (values[1] > max_x || values[2] > max_y)
    return;
  g_players.push_back(new Player(values[1], values[2], team, values[0]));
  i = 0;
  for (unsigned int k = 0; k < g_players.size(); ++k)
    {
      if (g_players[i]->getValue() == values[0])
	i = k;
    }
  if (static_cast<unsigned int>(i) >= g_players.size()
      || (static_cast<unsigned int>(i) < g_players.size() 
	  && g_players[i]->getValue() != values[0]))
    return;
  switch (values[3])
    {
    case 1:
      g_players[i]->setOrientation(UP);
      break;
    case 2:
      g_players[i]->setOrientation(RIGHT);
      break;
    case 3:
      g_players[i]->setOrientation(DOWN);
      break;
    case 4:
      g_players[i]->setOrientation(LEFT);
      break;		
    }
}


void 					ServerCom::player_incant(std::string const &instruction)
{
  unsigned int		j;
  int		  	idx = 0;
  int          		i = 4;
  int          		y = 4;
  std::vector<int>	values;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      for (j = 0; j < g_players.size(); ++j) {
	if (cut(instruction, y, i) == g_players[j]->getValue() && idx <= 4)
	  g_players[j]->setIncantation(true);
      }
      i += 1;
      y = i;
      idx++;
    }
}

void								ServerCom::player_inventory(std::string const &instruction)
{
  int          					i = 4;
  int           					y = 4;
  std::vector<int>				values;
  std::vector<Player *>::iterator it;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;
    }
  values.push_back(cut(instruction, y, instruction.length()));
  if (values[1] >= max_x || values[2] >= max_y)
    return;
  i = 0;
  while (static_cast<unsigned int>(i) < g_players.size()
	 && g_players[i]->getValue() != values[0] && g_players[i])
    i++;
  if (static_cast<unsigned int>(i) >= g_players.size()
      || (static_cast<unsigned int>(i) < g_players.size() 
	  && g_players[i]->getValue() != values[0]))
    return;
  g_players[i]->setX(values[1]);
  g_players[i]->setY(values[2]);
  g_players[i]->setFood(values[3]);
  g_players[i]->setLinemate(values[4]);
  g_players[i]->setDeraumere(values[5]);
  g_players[i]->setSibur(values[6]);
  g_players[i]->setMendiane(values[7]);
  g_players[i]->setPhiras(values[8]);
  g_players[i]->setThystame(values[9]);
}
