//
// network.cpp for graphic_client in /home/eyzat_f/projets/psu/zappy/graphic
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Mon Jun 23 15:44:42 2014 florian eyzat
// Last update Sun Jul 13 14:25:38 2014 florian eyzat
//

#include "network.hpp"

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

// FILE	*sock;
// int			sockfd;
// fd_set		readfds;
// struct sockaddr_in	server;

Network::Network()
{
}

Network::~Network()
{
}

bool	Network::connectToServ(char *serv, int port)
{
  sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  server.sin_family = AF_INET;
  if (inet_aton(serv, &(server.sin_addr)) == 0)
    return (false);
  server.sin_port = htons(port);
  if (connect(sockfd, (struct sockaddr *)&server, sizeof(server)) == -1)
    {
      std::cerr << "./graphic_client : connect failed" << std::endl;
      return (false);
    }
  std::cout << "Connexion established" << std::endl;
  return (true);
}

const char	*Network::recupInfo()
{
  int		ret;

  struct timeval tv = {0, 10000};

  FD_ZERO(&readfds);
  FD_SET(0, &readfds);
  FD_SET(sockfd, &readfds);
  ret = select(sockfd+1, &readfds, 0, 0, &tv);
  if (ret == -1)
    {
      perror("select");
      return (NULL);
    }
  else if (ret == 1)
    return (receive_it());
  // perror("select");
  // std::cout << "no data available : " << ret << std::endl;
  return (NULL);
}

char	tmp[1];
std::string test;

const char	*Network::receive_it()
{
  int		ret;
  bool	ex = true;
  std::string	buf;
  

  tmp[0] = '\0';
  while (ex == true)
    {
      if(FD_ISSET(sockfd,&readfds) && ex == true)
	{
	  if ((ret = read(sockfd, tmp, 1)) <= 0)
	    {
	      std::cerr << "Connexion with server closed" << std::endl;
	      // if (ret == -1)
	      return ("CLOSE\n");
	      // if (ex == true)
	      // ex = false;
	    }
	  else
	    {
	      buf += tmp;
	      if (tmp[0] == '\n')
		ex = false;
	    }
	}
      else
	return ("CLOSE\n");
      tmp[0] = '\0';
    }
  test = buf;
  if (test == "BIENVENUE\n" || test == "BIENVENUE")
    {
      send(sockfd, "GRAPHIC\n", 1024, 0);
    }
  return (test.c_str());
}
