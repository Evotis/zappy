//
// Player.cpp for graphic_client in /home/eyzat_f/projets/psu/zappy/psu_2013_zappy/2d_client
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Tue Jul  1 14:01:33 2014 florian eyzat
// Last update Sun Jul 13 20:50:30 2014 florian eyzat
//

#include "Player.hpp"

std::vector<Player *>	g_players;

Player::Player(int _x, int _y, std::string _team, unsigned int _value)
  : x(_x), y(_y), incantation(false), nbFood(0),
    nbLinemate(0), nbDeraumere(0), nbSibur(0),
    nbMendiane(0), nbThystame(0), orientation(DOWN), value(_value)
{
  team = _team;
  alive = true;
  std::cerr << "New player nb " << value << " created in x = " << getX() << " and y = " << getY() << std::endl;
  std::cout << "New player nb " << value << " created in x = " << getX() << " and y = " << getY() << std::endl;
}

Player::~Player() {}

////////////////////////
//     getters        //
////////////////////////

std::string     Player::getTeamName() const
{
  return (team);
}

unsigned short	Player::getFood() const
{
  return (nbFood);
}

unsigned short	Player::getLinemate() const
{
  return (nbLinemate);
}

unsigned short	Player::getDeraumere() const
{
  return (nbDeraumere);
}

unsigned short	Player::getSibur() const
{
  return (nbSibur);
}

unsigned short	Player::getMendiane() const
{
  return (nbMendiane);
}

unsigned short	Player::getPhiras() const
{
  return (nbPhiras);
}

unsigned short	Player::getThystame() const
{
  return (nbThystame);
}

bool	Player::getIncantation() const
{
  return (incantation);
}

orient	Player::getOrientation() const
{
  return (orientation);
}

int	Player::getX() const
{
  return (x);
}

int	Player::getY() const
{
  return (y);
}

int	Player::getValue() const
{
  return (value);
}

////////////////////////
//     setters        //
////////////////////////

void	Player::setFood(unsigned short ref)
{
  nbFood = ref;
}

void	Player::setLinemate(unsigned short ref)
{
  nbLinemate = ref;
}

void	Player::setDeraumere(unsigned short ref)
{
  nbDeraumere = ref;
}

void	Player::setSibur(unsigned short ref)
{
  nbSibur = ref;
}

void	Player::setMendiane(unsigned short ref)
{
  nbMendiane = ref;
}

void	Player::setPhiras(unsigned short ref)
{
  nbPhiras = ref;
}

void	Player::setThystame(unsigned short ref)
{
  nbThystame = ref;
}

void	Player::setIncantation(bool ref)
{
  incantation = ref;
}

void	Player::setOrientation(orient ref)
{
  orientation = ref;
}

void	Player::setX(int ref)
{
  x = ref;
}

void	Player::setY(int ref)
{
  y = ref;
}

////////////////////////
//      others        //
////////////////////////

void	Player::putEgg()
{
}

void	Player::getThing(element)
{
}

void	Player::throwThing(element)
{
}
