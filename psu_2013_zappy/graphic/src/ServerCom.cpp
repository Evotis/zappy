#include <vector>
#include <sstream>
#include "ServerCom.hpp"

ServerCom::ServerCom()
{
  instr["pin"] = &ServerCom::player_inventory;
  instr["pic"] = &ServerCom::player_incant;
  instr["pfk"] = &ServerCom::player_egg;
  instr["pnw"] = &ServerCom::new_player;
  instr["ppo"] = &ServerCom::player_pos;
  instr["plv"] = &ServerCom::player_level;
  instr["pex"] = &ServerCom::expulsion;
  instr["pdi"] = &ServerCom::player_starvation;
  instr["pbc"] = &ServerCom::player_broadcast;
  instr["pie"] = &ServerCom::player_incant_end;
  instr["pdr"] = &ServerCom::player_drop;
  instr["pgt"] = &ServerCom::player_take;
  instr["enw"] = &ServerCom::egg_laid;
  instr["eht"] = &ServerCom::egg_birth;
  instr["ebo"] = &ServerCom::egg_die;
  instr["edi"] = &ServerCom::egg_die;
  instr["bct"] = &ServerCom::map_case;
  instr["msz"] = &ServerCom::map_size;
}

ServerCom::~ServerCom()
{}

ServerCom::ServerCom(ServerCom const &)
{}


ServerCom &ServerCom::operator=(ServerCom const &)
{
	return (*this);
}

void	ServerCom::com(std::string const &instruction)
{
  std::string 	tmp;
  std::map<std::string, void (ServerCom::*)(std::string const &)>::iterator it;

  tmp = instruction.substr(0, 3);
  if ((it = this->instr.find(tmp)) != this->instr.end())
    (this->*(instr[tmp]))(instruction);
}


int             	cut(std::string line, int x, int y)
{
  std::string         ret;
  int                 value;

  while (x <= y)
    {
      ret += line[x];
      ++x;
    }
  std::istringstream buffer(ret);
  buffer >> value;
  return (value);
}

void 								ServerCom::egg_laid(std::string const &instruction)
{
  (void)instruction;
  // int 							playernum;
  // std::vector<Player *>::iterator it;

  // playernum = cut(instruction, 4, instruction.length());
  // while ((*it) != g_players[playernum])
  //   it += 1;
  // if ((*it) != g_players[playernum])
  //   return;
  // (*it)->putEgg();
}

void 							ServerCom::team_names(std::string const &instruction)
{
  int   		    	   		i = 4;
  std::vector<std::string>	values;
  int 						posx = 0;
  std::string					ret;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      while (posx <= i)
	{
	  ret += instruction[posx];
	  ++posx;
	}
      values.push_back(ret);
      ret.clear();
      i += 1;
    }
}

void 					ServerCom::map_case(std::string const &instruction)
{
  int 	         		i = 4;
  int           		y = 4;
  std::vector<int>		box(7);
  std::vector<int>		values;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;
    }
  values.push_back(cut(instruction, y, instruction.length()));
  if (values[1] >= max_x || values[2] >= max_y)
    return;
  recup_case(values[0], values[1], &box);
  box[0] = values[2];
  box[1] = values[3];
  box[2] = values[4];
  box[3] = values[5];
  box[4] = values[6];
  box[5] = values[7];
  box[6] = values[8];
  modif_case(values[0], values[1], &box);
}

void	 				ServerCom::egg_die(std::string const &instruction)
{
  int 				eggnum;

  eggnum = cut(instruction, 4, instruction.length());
  (void) eggnum;
  // g_egg[eggnum] = false;
}
