//
// Egg.cpp for graphic_client in /home/eyzat_f/projets/psu/zappy/psu_2013_zappy/2d_client
// 
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
// 
// Started on  Fri Jul  4 12:50:06 2014 florian eyzat
// Last update Fri Jul  4 13:03:07 2014 florian eyzat
//

#include "Egg.hpp"

std::vector<Egg *>	g_egg;

Egg::Egg(int _x, int _y)
  : x(_x), y(_y), alive(false)
{
}

Egg::~Egg() {}

int	Egg::getX() const
{
  return (x);
}

int	Egg::getY() const
{
  return (y);
}

void	Egg::setX(int ref)
{
  x = ref;
}

void	Egg::setY(int ref)
{
  y = ref;
}

bool	Egg::isTurned() const
{
  return (alive);
}
