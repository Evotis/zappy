#include <vector>
#include "ServerCom.hpp"

void 								ServerCom::player_take(std::string const &instruction)
{
  int       	   					i = 4;
  int           					y = 4;
  std::vector<int>				values;
  int 							posx;
  int 							posy;
  std::vector<int>				box(7);

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;
    }
  values.push_back(cut(instruction, y, instruction.length()));
  i = 0;
  if (!values.size())
    return ;
  while (static_cast<unsigned int>(i) < g_players.size()
	 && g_players[i]->getValue() != values[0] && g_players[i])
    i++;
  if (static_cast<unsigned int>(i) >= g_players.size()
      || (static_cast<unsigned int>(i) < g_players.size() 
	  && g_players[i]->getValue() != values[0]))
    return;
  posx = g_players[i]->getX();
  posy = g_players[i]->getY();
  recup_case(posx, posy, &box);
  switch (values[1])
    {
    case 0:
      g_players[i]->setFood(g_players[i]->getFood() + 1);
      if (box[0])
	box[0] -= 1;
      break;
    case 1:
      g_players[i]->setLinemate(g_players[i]->getLinemate() + 1);
      if (box[1])
	box[1] -= 1;
      break;
    case 2:
      g_players[i]->setDeraumere(g_players[i]->getDeraumere() + 1);
      if (box[2])
	box[2] -= 1;
      break;
    case 3:
      g_players[i]->setSibur(g_players[i]->getSibur() + 1);
      if (box[3])
	box[3] -= 1;
      break;
    case 4:
      g_players[i]->setMendiane(g_players[i]->getMendiane() + 1);
      if (box[4])
	box[4] -= 1;
      break;
    case 5:
      g_players[i]->setPhiras(g_players[i]->getPhiras() + 1);
      if (box[5])
	box[5] -= 1;
      break;
    case 6:
      g_players[i]->setThystame(g_players[values[1]]->getThystame() + 1);
      if (box[6])
	box[6] -= 1;
      break;
    }
  modif_case(posx, posy, &box);
}

void 								ServerCom::player_drop(std::string const &instruction)
{
  int 							idx = 0;
  int       		   				i = 4;
  int          		 			y = 4;
  int 							posx;
  int 							posy;
  std::vector<int>				box(7);
  std::vector<int>				values;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;
      idx++;
    }
  values.push_back(cut(instruction, y, instruction.length()));
  i = 0;
  while (static_cast<unsigned int>(i) < g_players.size()
	 && g_players[i]->getValue() != values[0] && g_players[i])
    i++;
  if (static_cast<unsigned int>(i) >= g_players.size()
      || (static_cast<unsigned int>(i) < g_players.size()
	  && g_players[i]->getValue() != values[0]))
    return;
  posx = g_players[i]->getX();
  posy = g_players[i]->getY();
  recup_case(posx, posy, &box);
  switch (values[1])
    {
    case 0:
      g_players[i]->setFood(g_players[i]->getFood() - 1);
      box[0] += 1;
      break;
    case 1:
      g_players[i]->setLinemate(g_players[i]->getLinemate() - 1);
      box[1] += 1;
      break;
    case 2:
      g_players[i]->setDeraumere(g_players[i]->getDeraumere() - 1);
      box[2] += 1;
      break;
    case 3:
      g_players[i]->setSibur(g_players[i]->getSibur() - 1);
      box[3] += 1;
      break;
    case 4:
      g_players[i]->setMendiane(g_players[i]->getMendiane() - 1);
      box[4] += 1;
      break;
    case 5:
      g_players[i]->setPhiras(g_players[i]->getPhiras() - 1);
      box[5] += 1;
      break;
    case 6:
      g_players[i]->setThystame(g_players[i]->getThystame() - 1);
      box[6] += 1;
      break;
    }
  modif_case(posx, posy, &box);
}

void 								ServerCom::player_incant_end(std::string const &instruction)
{
  unsigned int					j;
  int 						idx = 0;
  int       	   				i = 4;
  int           				y = 4;
  std::vector<int>				values;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      if (idx <= 4)
  	{
	  for (j = 0; j < g_players.size(); ++j) {
	    if (g_players[j]->getValue() == cut(instruction, y, i))
	      g_players[j]->setIncantation(false);
	  }
  	}
      i += 1;
      y = i;
      idx++;
    }
}

void									ServerCom::player_starvation(std::string const &instruction)
{
  int 								playernum;
  unsigned int							i = 0;

  playernum = cut(instruction, 4, instruction.length());
  while (i < g_players.size() && g_players[i]->getValue() != playernum && g_players[i])
    i++;
  if (i >= g_players.size() || g_players[i]->getValue() != playernum)
    return;
  g_players[i]->alive = false;
}

void 					ServerCom::map_size(std::string const &instruction)
{
  int       	   		i = 4;
  int           		y = 4;
  std::vector<int>	values;

  while ((i = instruction.find(" ", i)) != static_cast<int>(std::string::npos))
    {
      values.push_back(cut(instruction, y, i));
      i += 1;
      y = i;
    }
  values.push_back(cut(instruction, y, instruction.length()));
  max_y = values[0];
  max_x = values[1];
  std::vector<std::vector<std::vector<int > > >tmp(max_x, std::vector<std::vector<int> >(max_y, std::vector<int>(7, 0)));

  std::cerr << "initializing the map... ";
  _map = tmp;
  std::cerr << "DONE" << std::endl;
}
