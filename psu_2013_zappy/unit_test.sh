#!/bin/bash

tests=(
"./zappy_world -p 9999 -x 1 -y 2 -n 'coucou' 'les' 'gens' -c 3 -t 4 -d"
"./zappy_world -p -x 1 -y 2 -n 'coucou' 'les' 'gens' -c 3 -t 4 -d"
"./zappy_world -p 9999 -x -y 2 -n 'coucou' 'les' 'gens' -c 3 -t 4 -d"
"./zappy_world -p 9999 -x 1 -y -n 'coucou' 'les' 'gens' -c 3 -t 4 -d"
"./zappy_world -p 9999 -x 1 -y 2 -n -c 4 -t 5 -d"
"./zappy_world -p 9999 -x 1 -y 2 -n 'coucou' 'les' 'gens' -c -t 4 -d"
"./zappy_world -p 9999 -x 1 -y 2 -n 'coucou' 'les' 'gens' -c 3 -t -d"
)

results=(
"testing arguments -- results should be the ones above"
"testing arguments -- results should be no port given"
"testing arguments -- results should be x not valid"
"testing arguments -- results should be y not valid"
"testing arguments -- results should be team name not valid"
"testing arguments -- results should be no nb_client given"
"testing arguments -- results should be time not given"
)

if [ $# -eq 1 ] && [ $1 = "-h" ] || [ $# -eq 0 ]
then
  echo "Usage: ./unit_test.sh all|see|[test n°]"
  echo "all     - run all available tests"
  echo "see     - display all available tests"
  echo "test n° - run test n°"
  exit
fi

if [ $# -ge 1 ]
then
  if [ $1 = "all" ]
  then
    for i in ${!tests[*]}
    do
      # display test
      echo "---------------------------------------------------------------------------------------------------"
      echo ${tests[$i]}
      echo

      # perfom
      ${tests[$i]}

      echo  "exit: $?"
      echo ${results[$i]}
      echo "---------------------------------------------------------------------------------------------------"
      echo
    done
  fi

  if [ $1 = "see" ]
  then
    for i in ${!tests[*]}
    do
      echo $i: ${tests[$i]}
    done
    exit
  fi

  if [ $# -eq 1 ] && [ $1 -eq $1 ] 2> /dev/null
  then
    # display test
    echo ${tests[$2]}
    echo ${results[$2]}
    echo

    # perfom
    ${tests[$2]}
    echo "exit: $?"
  fi

  exit
fi
